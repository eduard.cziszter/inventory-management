using InventoryManagementBackend.Core.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using File = InventoryManagementBackend.Core.Entities.File;

namespace InventoryManagementBackend.Infrastructure.Database;

public class AppDbContext : IdentityDbContext<ApplicationUser>
{
    public DbSet<File> Files { get; set; }
    public DbSet<Location> Locations { get; set; }
    public DbSet<Item> Items { get; set; }
    public DbSet<Organization> Organizations { get; set; }
    public DbSet<Department> Departments { get; set; }
    public DbSet<Request> Requests { get; set; }
    public DbSet<Inventory> Inventories { get; set; }
    public DbSet<Setting> Settings { get; set; }
    public DbSet<UserOrganization> UserOrganizations { get; set; }
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
        
    }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Item>()
            .Property(i => i.IsEditItem)
            .HasDefaultValue(false);
        base.OnModelCreating(builder);
    }
}
using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IDepartmentsRepository : IRepository<Department>
{
    Task<IEnumerable<Department>> GetForOrgIdAsync(string orgId, CancellationToken cancellationToken = default);
}
using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IItemCategoriesRepository : IRepository<ItemCategory>
{
    
}
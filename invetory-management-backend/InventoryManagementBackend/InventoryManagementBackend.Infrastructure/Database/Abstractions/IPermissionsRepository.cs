using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IPermissionsRepository : IRepository<Permission>;
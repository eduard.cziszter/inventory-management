using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IInventoriesRepository : IRepository<Inventory>
{
    Task<IEnumerable<Inventory>> GetByOrganizationSlug(string orgSlug, CancellationToken cancellationToken = default);
}
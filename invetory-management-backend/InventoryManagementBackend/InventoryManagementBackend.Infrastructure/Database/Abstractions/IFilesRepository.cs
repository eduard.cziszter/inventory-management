using File = InventoryManagementBackend.Core.Entities.File;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IFilesRepository : IRepository<File>;
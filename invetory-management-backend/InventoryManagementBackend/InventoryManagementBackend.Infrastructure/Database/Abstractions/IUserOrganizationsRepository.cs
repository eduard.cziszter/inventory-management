using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IUserOrganizationsRepository : IRepository<UserOrganization>
{
    public Task<IEnumerable<UserOrganization>> GetOrganizationsForUserAsync(string userId, CancellationToken cancellationToken = default);
}
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using InventoryManagementBackend.Core.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IRepository<TEntity> where TEntity : BaseEntity
{
    DbSet<TEntity> DbSet { get; }
    Task<TEntity> AddAsync([NotNull] TEntity entity, CancellationToken cancellationToken = default);
    Task AddAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);
    Task<bool> HasAsync(string id, CancellationToken cancellationToken = default);
    Task<bool> HasAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default);
    Task<TEntity?> GetAsync(string id, CancellationToken cancellationToken = default);
    Task<TEntity?> GetAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default);
    Task<IEnumerable<TEntity>> GetAsync(CancellationToken cancellationToken = default);
    Task AddOrUpdateAsync([NotNull] TEntity entity, CancellationToken cancellationToken = default);
    Task AddOrUpdateAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);
    Task UpdateAsync(string id, object entity, CancellationToken cancellationToken = default);
    Task UpdateAsync(IEnumerable<TEntity> newEntities, CancellationToken cancellationToken = default);
    Task<TEntity?> DeleteAsync(string id, CancellationToken cancellationToken = default);
    Task<TEntity?> DeleteAsync(TEntity entity, CancellationToken cancellationToken = default);
    Task<IEnumerable<TEntity>> DeleteAsync(IEnumerable<string> ids, CancellationToken cancellationToken = default);
    Task DeleteAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);
    Task<int> SaveAsync(CancellationToken cancellationToken = default);
}
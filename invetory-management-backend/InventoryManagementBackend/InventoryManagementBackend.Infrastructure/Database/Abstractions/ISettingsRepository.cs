using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface ISettingsRepository : IRepository<Setting>
{
    
}
using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IItemsRepository : IRepository<Item>
{
    Task<IEnumerable<Item>> GetByInventoryIdAsync(string inventoryId, CancellationToken cancellationToken = default);
}

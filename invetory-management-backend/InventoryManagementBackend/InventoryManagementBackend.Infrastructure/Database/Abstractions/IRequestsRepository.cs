using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IRequestsRepository : IRepository<Request>
{
    public Task<IEnumerable<Request>> GetRequestsByUserId(string userId);
    public Task<IEnumerable<Request>> GetRequestsByApproverId(string approverId);
}
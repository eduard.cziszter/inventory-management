using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Database.Abstractions;

public interface IOrganizationsRepository : IRepository<Organization>
{
    public Task<IEnumerable<Organization>> GetByIds(IEnumerable<string> ids);
}
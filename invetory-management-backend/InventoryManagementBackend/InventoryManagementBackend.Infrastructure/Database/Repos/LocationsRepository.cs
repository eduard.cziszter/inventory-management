using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class LocationsRepository(AppDbContext context) : Repository<Location>(context), ILocationsRepository;
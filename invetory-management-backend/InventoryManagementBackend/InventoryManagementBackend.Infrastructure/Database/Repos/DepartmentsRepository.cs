using System.Linq.Expressions;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class DepartmentsRepository(AppDbContext context) : Repository<Department>(context), IDepartmentsRepository
{
    public async Task<IEnumerable<Department>> GetAsync(CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Organization)
            .ToListAsync(cancellationToken);
    }

    public async Task<Department?> GetAsync(Expression<Func<Department, bool>> predicate, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Organization)
            .FirstOrDefaultAsync(predicate, cancellationToken);
    }
    
    public async Task<IEnumerable<Department>> GetForOrgIdAsync(string orgId, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Where(d => d.OrganizationId == orgId)
            .ToListAsync(cancellationToken);
    }
}
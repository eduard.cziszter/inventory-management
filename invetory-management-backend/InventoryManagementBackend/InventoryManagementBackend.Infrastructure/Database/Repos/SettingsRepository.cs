using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class SettingsRepository(AppDbContext context) : Repository<Setting>(context), ISettingsRepository;
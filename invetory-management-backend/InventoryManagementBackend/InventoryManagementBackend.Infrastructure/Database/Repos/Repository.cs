using System.Linq.Expressions;
using InventoryManagementBackend.Core.Abstractions;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
{
    private readonly AppDbContext _context;

    public Repository(AppDbContext context) {
        _context = context;
        DbSet = _context.Set<TEntity>();
    }
    
    public DbSet<TEntity> DbSet { get; }
    
    public async Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken = default)
    {
        entity.Created = DateTime.Now;
        entity.Updated = DateTime.Now;
        
        entity = (await DbSet.AddAsync(entity, cancellationToken)).Entity;
        await SaveAsync(cancellationToken);

        return entity;
    }

    public async Task AddAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
    {
        foreach (var entity in entities)
        {
            entity.Created = entity.Updated = DateTime.Now;
        }
        
        await DbSet.AddRangeAsync(entities, cancellationToken);
        await SaveAsync(cancellationToken);
    }

    public async Task<bool> HasAsync(string id, CancellationToken cancellationToken = default)
    {
        return await DbSet.AnyAsync(e => e.Id == id, cancellationToken);
    }

    public async Task<bool> HasAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default)
    {
        return await DbSet.AnyAsync(predicate, cancellationToken);
    }

    public async Task<TEntity?> GetAsync(string id, CancellationToken cancellationToken = default)
    {
        return await DbSet.FirstOrDefaultAsync(e => e.Id.Equals(id), cancellationToken);
    }

    public async Task<TEntity?> GetAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default)
    {
        return await DbSet.FirstOrDefaultAsync(predicate, cancellationToken);
    }

    public async Task<IEnumerable<TEntity>> GetAsync(CancellationToken cancellationToken = default)
    {
        return await DbSet.ToListAsync(cancellationToken);
    }

    public async Task AddOrUpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
    {
        var existing = await DbSet.FirstOrDefaultAsync(item => item.Id.Equals(entity.Id), cancellationToken);
        
        if (existing is null)
        {
            await AddAsync(entity, cancellationToken);
        }
        
        await UpdateAsync(entity.Id, entity, cancellationToken);
    }

    public async Task AddOrUpdateAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
    {
        var entitiesList = entities.ToList();
        var entitiesIds = entitiesList.Select(e => e.Id).ToList();
        var existing = (await DbSet.Select(e => e.Id).ToListAsync(cancellationToken))
            .Where(id => entitiesIds.Contains(id)).ToList();
        
        var entitiesToAdd = entitiesList.Where(e => !existing.Contains(e.Id)).ToList();
        if (entitiesToAdd.Count != 0)
        {
            await DbSet.AddRangeAsync(entities, cancellationToken);
        }
        
        var entitiesToUpdate = entitiesList.Where(e => existing.Contains(e.Id)).ToList();
        if (entitiesToUpdate.Count != 0)
        {
            await UpdateAsync(entitiesToUpdate, cancellationToken);
        }

        await SaveAsync(cancellationToken);
    }

    public async Task UpdateAsync(string id, object newEntity, CancellationToken cancellationToken = default)
    {
        var entity = await GetAsync(id, cancellationToken);
        if (entity is null)
        {
            return;
        }

        entity.Updated = DateTime.Now;
        CheckUpdateObject(entity, newEntity);

        await SaveAsync(cancellationToken);
    }

    public async Task UpdateAsync(IEnumerable<TEntity> newEntities, CancellationToken cancellationToken = default)
    {
        var newEntitiesList = newEntities.ToList();
        var newEntitiesIds = newEntitiesList.Select(n => n.Id);
        var existingEntities = await DbSet.Where(e => newEntitiesIds.Contains(e.Id)).ToListAsync(cancellationToken);
        foreach (var newEntity in newEntitiesList)
        {
            var existingEntity = existingEntities.FirstOrDefault(e => e.Id.Equals(newEntity.Id));
            if (existingEntity is null)
            {
                Console.WriteLine($"Entity with id ${newEntity.Id} not found");
                continue;
            }

            CheckUpdateObject(existingEntity, newEntity);
        }
    }

    public async Task<TEntity?> DeleteAsync(string id, CancellationToken cancellationToken = default)
    {
        var entity = await DbSet.FirstOrDefaultAsync(item => item.Id.Equals(id), cancellationToken);

        if (entity is null)
        {
            return null;
        }

        entity = DbSet.Remove(entity).Entity;
        await SaveAsync(cancellationToken);

        return entity;
    }

    public async Task<TEntity?> DeleteAsync(TEntity entity, CancellationToken cancellationToken = default)
    {
        var deleted = DbSet.Remove(entity).Entity;
        await SaveAsync(cancellationToken);

        return deleted;
    }

    public async Task<IEnumerable<TEntity>> DeleteAsync(IEnumerable<string> ids, CancellationToken cancellationToken = default)
    {
        var entities = await DbSet.Where(e => ids.Contains(e.Id)).ToArrayAsync(cancellationToken);

        DbSet.RemoveRange(entities);
        await SaveAsync(cancellationToken);

        return entities;
    }

    public async Task DeleteAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
    {
        DbSet.RemoveRange(entities);
        await SaveAsync(cancellationToken);
    }

    public Task<int> SaveAsync(CancellationToken cancellationToken = default)
    {
        return _context.SaveChangesAsync(cancellationToken);
    }
    
    private static void CheckUpdateObject(TEntity originalObj, object updateObj) {
        foreach (var property in updateObj.GetType().GetProperties())
        {
            if (property.Name.Equals("Id")) continue;
            var propValue = property.GetValue(updateObj, null);
            var originalProp = originalObj.GetType().GetProperty(property.Name);
            if (propValue is not null && originalProp is not null)
            {
                originalProp.SetValue(originalObj, propValue);
            }
        }
    }
}
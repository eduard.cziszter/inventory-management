using System.Linq.Expressions;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class InventoriesRepository(AppDbContext context) : Repository<Inventory>(context), IInventoriesRepository
{
    public async Task<Inventory?> GetAsync(Expression<Func<Inventory, bool>> predicate, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Organization)
            .Include(i => i.Owner)
            .FirstOrDefaultAsync(predicate, cancellationToken);
    }

    public async Task<IEnumerable<Inventory>> GetAsync(CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Organization)
            .Include(i => i.Owner)
            .ToListAsync(cancellationToken);
    }

    public async Task<IEnumerable<Inventory>> GetByOrganizationSlug(string orgSlug, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Owner)
            .Where(i => i.Organization.Slug == orgSlug)
            .ToListAsync(cancellationToken);
    }
}
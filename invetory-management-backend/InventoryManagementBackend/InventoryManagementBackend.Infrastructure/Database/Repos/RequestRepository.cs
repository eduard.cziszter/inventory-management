using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class RequestRepository(AppDbContext context) : Repository<Request>(context), IRequestsRepository
{
    public async Task<Request?> GetAsync(string id, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(r => r.Item)
                .ThenInclude(i => i.Location)
            .Include(r => r.Item)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.Item)
                .ThenInclude(i => i.Image)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Location)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Image)
            .FirstOrDefaultAsync(e => e.Id.Equals(id), cancellationToken);
    }

    public async Task<IEnumerable<Request>> GetAsync(CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(r => r.Item)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.User)
            .ToListAsync(cancellationToken);
    }

    public async Task<IEnumerable<Request>> GetRequestsByUserId(string userId)
    {
        return await DbSet
            .Include(r => r.Item)
                .ThenInclude(i => i.Location)
            .Include(r => r.Item)
                .ThenInclude(i => i.Image)
            .Include(r => r.Item)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Location)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Image)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.Approver)
            .Where(r => r.UserId == userId)
            .ToListAsync();
    }

    public async Task<IEnumerable<Request>> GetRequestsByApproverId(string approverId)
    {
        return await DbSet
            .Include(r => r.Item)
                .ThenInclude(i => i.Location)
            .Include(r => r.Item)
                .ThenInclude(i => i.Image)
            .Include(r => r.Item)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Location)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Image)
            .Include(r => r.NewItem)
                .ThenInclude(i => i.Inventory)
            .Include(r => r.Approver)
            .Where(r => r.ApproverId == approverId)
            .ToListAsync();
    }
}
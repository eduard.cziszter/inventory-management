using System.Linq.Expressions;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class OrganizationsRepository(AppDbContext context) : Repository<Organization>(context), IOrganizationsRepository
{
    public async Task<IEnumerable<Organization>> GetByIds(IEnumerable<string> ids)
    { 
        return await DbSet.Where(o => ids.Contains(o.Id)).ToListAsync();
    }

    public async Task<Organization?> GetAsync(Expression<Func<Organization, bool>> predicate, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(o => o.Logo)
            .FirstOrDefaultAsync(predicate, cancellationToken);
    }

    public async Task<IEnumerable<Organization>> GetAsync(CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(o => o.Logo)
            .ToListAsync(cancellationToken);
    }
}
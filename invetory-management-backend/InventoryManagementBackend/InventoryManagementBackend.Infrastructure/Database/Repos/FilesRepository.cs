using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using File = InventoryManagementBackend.Core.Entities.File;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class FilesRepository(AppDbContext context) : Repository<File>(context), IFilesRepository;
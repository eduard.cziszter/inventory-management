using System.Linq.Expressions;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class ItemsRepository(AppDbContext context) : Repository<Item>(context), IItemsRepository
{
    public async Task<Item?> GetAsync(Expression<Func<Item, bool>> predicate, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Inventory)
            .Include(i => i.Image)
            .Include(i => i.Location)
            .FirstOrDefaultAsync(predicate, cancellationToken);
    }

    public async Task<IEnumerable<Item>> GetAsync(CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Inventory)
            .Include(i => i.Image)
            .Include(i => i.Location)
            .Where(i => !i.IsEditItem)
            .ToListAsync(cancellationToken);
    }

    public async Task<Item?> GetAsync(string id, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Inventory)
            .Include(i => i.Image)
            .Include(i => i.Location)
            .FirstOrDefaultAsync(i => i.Id.Equals(id), cancellationToken);
    }

    public async Task<IEnumerable<Item>> GetByInventoryIdAsync(string inventoryId, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(i => i.Inventory)
            .Include(i => i.Image)
            .Include(i => i.Location)
            .Where(i => !i.IsEditItem) 
            .Where(i => i.InventoryId == inventoryId)
            .ToListAsync(cancellationToken);
    }
}
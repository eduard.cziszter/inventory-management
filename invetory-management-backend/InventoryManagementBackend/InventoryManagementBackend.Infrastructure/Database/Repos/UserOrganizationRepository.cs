using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class UserOrganizationRepository(AppDbContext context)
    : Repository<UserOrganization>(context), IUserOrganizationsRepository
{
    public async Task<IEnumerable<UserOrganization>> GetOrganizationsForUserAsync(string userId, CancellationToken cancellationToken = default)
    {
        return await DbSet
            .Include(uo => uo.Organization)
            .ThenInclude(o => o.Logo)
            .Include(uo => uo.Department)
            .Where(uo => uo.UserId == userId)
            .ToListAsync(cancellationToken);
    }
}
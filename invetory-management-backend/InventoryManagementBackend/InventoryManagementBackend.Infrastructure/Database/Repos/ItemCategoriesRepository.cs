using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class ItemCategoriesRepository(AppDbContext context)
    : Repository<ItemCategory>(context), IItemCategoriesRepository;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class CategoriesRepository(AppDbContext context) : Repository<Category>(context), ICategoriesRepository;
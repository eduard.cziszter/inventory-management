using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Database.Repos;

public class PermissionsRepository(AppDbContext context) : Repository<Permission>(context), IPermissionsRepository;
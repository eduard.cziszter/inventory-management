using InventoryManagementBackend.Core.Utils;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Razor.Templating.Core;
using IEmailSender = InventoryManagementBackend.Infrastructure.Services.Abstractions.IEmailSender;

namespace InventoryManagementBackend.Infrastructure.Services;

public class EmailSender : IEmailSender
{
    private readonly IConfiguration _configuration;

    public EmailSender(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task SendEmailAsync(string email, string subject, RequestEmail requestEmail)
    {
        var htmlMessage = await RazorTemplateEngine.RenderAsync("RequestEmailTemplate", requestEmail);
        
        var smtpClient = new SmtpClient();
        await smtpClient.ConnectAsync("smtp.gmail.com", 587, false).ConfigureAwait(false);
        smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
        
        var senderPassword = _configuration["Authentication:Mailing:Password"];
        await smtpClient.AuthenticateAsync("inventar@ligaac.ro", senderPassword);
        
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("Inventar Liga AC", "inventar@ligaac.ro"));
        mail.To.Add(new MailboxAddress("Inventar Liga AC", email));
        mail.Subject = subject;
        var body = new BodyBuilder();
        body.HtmlBody = htmlMessage;
        mail.Body = body.ToMessageBody();

        await smtpClient.SendAsync(mail);
    }
}
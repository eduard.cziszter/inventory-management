using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;

namespace InventoryManagementBackend.Infrastructure.Services;

public class ApproveRequestHelper : IApproveRequestHelper
{
    private readonly IRequestsRepository _requestsRepository;
    private readonly IItemsRepository _itemsRepository;
    
    public ApproveRequestHelper(IRequestsRepository requestsRepository, IItemsRepository itemsRepository)
    {
        _requestsRepository = requestsRepository;
        _itemsRepository = itemsRepository;
    }

    public async Task ApproveRequestAsync(Request request)
    {
        if (request.RequestType == RequestType.DeleteRequest)
        {
            var toBeDeletedItemId = request.Item.Id;
            await _itemsRepository.DeleteAsync(toBeDeletedItemId);

            var requests = await _requestsRepository.GetAsync();
            var ids = requests
                .Where(r => r.ItemId == toBeDeletedItemId)
                .Select(r => r.Id);

            await _requestsRepository.DeleteAsync(ids);
        }
        else if (request.RequestType == RequestType.EditRequest)
        {
            var toBeEditedItemId = request.Item.Id;
            request.NewItem.IsEditItem = false;
            await _itemsRepository.UpdateAsync(toBeEditedItemId, request.NewItem);
            await _itemsRepository.DeleteAsync(request.NewItem);
            await _requestsRepository.DeleteAsync(request);
        }
        else if (request.RequestType == RequestType.TransferRequest)
        {
            var itemNewQuantity = request.Item.Quantity - request.NewItem.Quantity;
            if (itemNewQuantity > 0)
            {
                request.Item.Quantity = itemNewQuantity;
                await _itemsRepository.UpdateAsync(request.Item.Id, request.Item);
            }
            else
            {
                await _itemsRepository.DeleteAsync(request.Item.Id);
            }
            
            request.NewItem.IsEditItem = false;
            if (request.NewItem.Quantity > 0)
            {
                await _itemsRepository.UpdateAsync(request.NewItem.Id, request.NewItem);
            }
            else
            {
                await _itemsRepository.DeleteAsync(request.NewItem.Id);
            }
            
            await _requestsRepository.DeleteAsync(request.Id);
        }
    }
}
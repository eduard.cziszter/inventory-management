using InventoryManagementBackend.Core.Dtos;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using File = InventoryManagementBackend.Core.Entities.File;

namespace InventoryManagementBackend.Infrastructure.Services;

public class CustomMapper : ICustomMapper
{
    public SettingDto MapSetting(Setting setting)
    {
        return new SettingDto
        {
            Slug = setting.Slug,
            Value = setting.Value
        };
    }

    public FileDto MapFile(File file)
    {
        return new FileDto
        {
            Id = file.Id,
            Name = file.Name,
            Path = file.Path,
            Extension = file.Extension,
            OriginalName = file.OriginalName
        };
    }

    public OrganizationDto MapOrganization(Organization organization)
    {
        return new OrganizationDto
        {
            Id = organization.Id,
            Slug = organization.Slug,
            Name = organization.Name,
            ShortName = organization.ShortName,
            Description = organization.Description,
            LogoPath = organization.Logo.Path
        };
    }

    public IEnumerable<OrganizationDto> MapOrganizations(IEnumerable<Organization> organizations)
    {
        return organizations.Select(o => new OrganizationDto
        {
            Id = o.Id,
            Slug = o.Slug,
            Name = o.Name,
            ShortName = o.ShortName,
            Description = o.Description,
            LogoPath = o.Logo.Path
        });
    }

    public ApplicationUserDto MapApplicationUser(ApplicationUser user)
    {
        return new ApplicationUserDto
        {
            FirstName = user.FirstName,
            LastName = user.LastName,
            Email = user.Email
        };
    }

    public ItemDto MapItem(Item item)
    {
        return new ItemDto
        {
            Id = item.Id,
            Name = item.Name,
            Description = item.Description,
            Quantity = item.Quantity,
            ImagePath = item.Image.Path,
            LocationId = item.Location.Id,
            LocationName = item.Location.Name,
            NeedsApproval = item.NeedsApproval,
            InventoryName = item.Inventory.Name
        };
    }

    public IEnumerable<ItemDto> MapItems(IEnumerable<Item> items)
    {
        return items.Select(i => new ItemDto
        {
            Id = i.Id,
            Name = i.Name,
            Description = i.Description,
            Quantity = i.Quantity,
            ImagePath = i.Image.Path,
            LocationId = i.Location.Id,
            LocationName = i.Location.Name,
            NeedsApproval = i.NeedsApproval,
            InventoryName = i.Inventory.Name
        });
    }

    public InventoryDto MapInventory(Inventory inventory, IEnumerable<Item> items)
    {
        var itemDtos = MapItems(items);
        return new InventoryDto
        {
            Id = inventory.Id,
            Name = inventory.Name,
            Description = inventory.Description,
            Slug = inventory.Slug,
            Items = itemDtos,
            OwnerId = inventory.OwnerId,
            OwnerEmail = inventory.Owner.Email
        };
    }

    public Inventory InventoryFromDto(InventoryPostDto inventoryDto)
    {
        return new Inventory
        {
            Name = inventoryDto.Name,
            Description = inventoryDto.Description,
            Slug = inventoryDto.Slug,
            OrganizationId = inventoryDto.OrganizationId
        };
    }

    public Item ItemFromDto(ItemPostDto itemDto)
    {
        return new Item
        {
            Name = itemDto.Name,
            Description = itemDto.Description,
            Quantity = itemDto.Quantity,
            ImageId = itemDto.ImageId,
            LocationId = itemDto.LocationId,
            InventoryId = itemDto.InventoryId,
            NeedsApproval = itemDto.NeedsApproval
        };
    }

    public IEnumerable<RequestDto> MapRequests(IEnumerable<Request> requests)
    {
        var requestDtos = new List<RequestDto>();
        
        foreach (var request in requests)
        {
            var requestDto = new RequestDto();

            requestDto.Id = request.Id;
            requestDto.Item = MapItem(request.Item);
            requestDto.NeedsApproval = request.NeedsApproval;
            requestDto.RequestType = _getRequestType(request.RequestType);
            requestDto.Description = request.Description;
            
            if (request.NewItem is null) requestDto.Item = MapItem(request.Item);
            else requestDto.NewItem = MapItem(request.NewItem);

            if (request.Approver is null) requestDto.ApproverEmail = string.Empty;
            else requestDto.ApproverEmail = request.Approver.Email;
            
            requestDtos.Add(requestDto);
        }
        
        return requestDtos;
    }

    public IEnumerable<OrganizationDto> OrganizationsFromUserOrganizations(IEnumerable<UserOrganization> userOrganizations)
    {
        return userOrganizations.Select(uo => new OrganizationDto
        {
            Id = uo.Organization.Id,
            Slug = uo.Organization.Slug,
            Name = uo.Organization.Name,
            ShortName = uo.Organization.ShortName,
            Description = uo.Organization.Description,
            LogoPath = uo.Organization.Logo.Path,
            DepartmentName = uo.Department.Name
        });
    }

    private string _getRequestType(RequestType requestType)
    {
        var requestDict = new Dictionary<RequestType, string>()
        {
            {RequestType.DeleteRequest, "Cerere de ștergere"},
            {RequestType.EditRequest, "Cerere de actualizare"},
            {RequestType.TransferRequest, "Cerere de transfer"},
        };
        
        return requestDict[requestType];
    }
}
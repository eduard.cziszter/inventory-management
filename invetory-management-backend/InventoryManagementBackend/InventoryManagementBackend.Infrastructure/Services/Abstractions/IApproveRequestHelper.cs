using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Infrastructure.Services.Abstractions;

public interface IApproveRequestHelper
{
    public Task ApproveRequestAsync(Request request);
}
using InventoryManagementBackend.Core.Dtos;
using InventoryManagementBackend.Core.Entities;
using File = InventoryManagementBackend.Core.Entities.File;

namespace InventoryManagementBackend.Infrastructure.Services.Abstractions;

public interface ICustomMapper
{
    public SettingDto MapSetting(Setting setting);
    public FileDto MapFile(File file);
    public OrganizationDto MapOrganization(Organization organization);
    public IEnumerable<OrganizationDto> MapOrganizations(IEnumerable<Organization> organizations);
    public ApplicationUserDto MapApplicationUser(ApplicationUser user);
    public ItemDto MapItem(Item item);
    public IEnumerable<ItemDto> MapItems(IEnumerable<Item> items);
    public InventoryDto MapInventory(Inventory inventory, IEnumerable<Item> items);
    public Inventory InventoryFromDto(InventoryPostDto inventoryDto);
    public Item ItemFromDto(ItemPostDto itemDto);
    public IEnumerable<RequestDto> MapRequests(IEnumerable<Request> requests);

    public IEnumerable<OrganizationDto> OrganizationsFromUserOrganizations(
        IEnumerable<UserOrganization> userOrganizations);
}
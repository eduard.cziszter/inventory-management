using InventoryManagementBackend.Core.Utils;

namespace InventoryManagementBackend.Infrastructure.Services.Abstractions;

public interface IIdentityService
{
    public Task<ActionResponse> GoogleSignIn(string token);
}
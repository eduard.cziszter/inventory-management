using InventoryManagementBackend.Infrastructure.Utils;
using Microsoft.AspNetCore.Http;
using File = InventoryManagementBackend.Core.Entities.File;

namespace InventoryManagementBackend.Infrastructure.Repositories.Abstractions;

public interface IUploadManager
{
    Task<ActionResponse<File>> Upload(IFormFile file);
}
using InventoryManagementBackend.Core.Utils;

namespace InventoryManagementBackend.Infrastructure.Services.Abstractions;

public interface IEmailSender
{
    public Task SendEmailAsync(string email, string subject, RequestEmail requestEmail);
}
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Google.Apis.Auth;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Infrastructure.Services;

public class IdentityService : IIdentityService
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IConfiguration _configuration;
    
    public IdentityService(UserManager<ApplicationUser> userManager, IConfiguration configuration)
    {
        _userManager = userManager;
        _configuration = configuration;
    }
    
    public async Task<ActionResponse> GoogleSignIn(string token)
    {
        var validationSettings = new GoogleJsonWebSignature.ValidationSettings()
            {IssuedAtClockTolerance = TimeSpan.FromSeconds(3600)};
        
        GoogleJsonWebSignature.Payload payload =
            await GoogleJsonWebSignature.ValidateAsync(token, validationSettings);
            
        var user = await _userManager.FindByEmailAsync(payload.Email);
        var rolesToAdd = new List<string> {"User", "Member"};

        if (user is null)
        {
            user = new ApplicationUser
            {
                UserName = payload.Email,
                Email = payload.Email,
                FirstName = payload.GivenName,
                LastName = payload.FamilyName,
                EmailConfirmed = true,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
                
            var createResult = await _userManager.CreateAsync(user);
            if (!createResult.Succeeded)
            {
                return new ActionResponse()
                {
                    Action = "GoogleSignIn",
                    Errors = new List<string> { createResult.Errors.Select(e => e.Description).ToString() }
                };
            }
                
            var addToRoleResult = await _userManager.AddToRolesAsync(user, rolesToAdd);
            if (!addToRoleResult.Succeeded)
            {
                return new ActionResponse()
                {
                    Action = "GoogleSignIn",
                    Errors = new List<string> { addToRoleResult.Errors.Select(e => e.Description).ToString() }
                };
            }
        }

        var roles = await _userManager.GetRolesAsync(user);
        if (roles.Contains("Admin"))
        {
            return new ActionResponse()
            {
                Action = "GoogleSignIn",
                Errors = new List<string> { "You can't use this account here. You can use it only in admin panel." }
            }; 
        }

        var session = new Session
        {
            Username = user.UserName,
            TokenType = "Bearer",
            Token = GenerateAuthenticationResult(user, rolesToAdd),
            Expiration = DateTime.UtcNow.AddDays(1),
            Roles = string.Join(",", rolesToAdd),
            Ext = true
        };
            
        return new ActionResponse<Session>{Action = "Login", Item = session};
    }

    private string GenerateAuthenticationResult(ApplicationUser newUser, List<string> roles)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_configuration["Authentication:JWT:Secret"]);
        Console.WriteLine(_configuration.GetSection("JWT:Issuer").Value);

        var claims = new List<Claim>();
        foreach (var role in roles)
        {
            claims.Add(new Claim(ClaimTypes.Role, role));
        }
        claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
        claims.Add(new Claim(JwtRegisteredClaimNames.Iss, _configuration["Authentication:JWT:Issuer"]));
        claims.Add(new Claim(JwtRegisteredClaimNames.Sub, newUser.UserName));
        
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Issuer = _configuration["Authentication:JWT:Issuer"],
            Expires = DateTime.UtcNow.AddDays(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);

        return tokenHandler.WriteToken(token);
    }
}
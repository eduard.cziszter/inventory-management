﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InventoryManagementBackend.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddedInventoryOrganizationFK : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrganizationId",
                table: "Inventories",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Inventories_OrganizationId",
                table: "Inventories",
                column: "OrganizationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Inventories_Organizations_OrganizationId",
                table: "Inventories",
                column: "OrganizationId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inventories_Organizations_OrganizationId",
                table: "Inventories");

            migrationBuilder.DropIndex(
                name: "IX_Inventories_OrganizationId",
                table: "Inventories");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "Inventories");
        }
    }
}

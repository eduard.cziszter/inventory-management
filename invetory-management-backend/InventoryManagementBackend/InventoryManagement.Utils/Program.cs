﻿// See https://aka.ms/new-console-template for more information

using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using File = System.IO.File;

static async Task Main()
{
    var services = new ServiceCollection();

    var fileName = "appsettings.Development.json";
    File.Copy(Path.Combine("../InventoryManagementBackend.Api", fileName), fileName, true);
    var configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.Development.json", optional: false, reloadOnChange: true)
        .Build();
    var connectionString = configuration.GetConnectionString("Default");

    File.Delete(fileName);

    services.AddDbContext<AppDbContext>(options => {
        options.UseNpgsql(connectionString, x =>
        {
            x.MigrationsAssembly("InventoryManagementBackend.Infrastructure");
        });
    });

    services.AddDefaultIdentity<ApplicationUser>()
        .AddRoles<IdentityRole>()
        .AddEntityFrameworkStores<AppDbContext>()
        .AddDefaultTokenProviders();
    
    var serviceProvider = services.BuildServiceProvider();
    var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
    var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
    
    await roleManager.CreateAsync(new IdentityRole("User"));
    await roleManager.CreateAsync(new IdentityRole("Admin"));
    await roleManager.CreateAsync(new IdentityRole("Member"));
    Console.WriteLine($"Roles created successfully!");

    Console.WriteLine("Enter email:");
    var email = Console.ReadLine();

    var user = new ApplicationUser
    {
        UserName = email,
        Email = email,
        FirstName = "Admin",
        LastName = "Admin",
        Created = DateTime.UtcNow,
        Updated = DateTime.UtcNow
    };

    var result = await userManager.CreateAsync(user);
    if (result.Succeeded)
    {
        Console.WriteLine($"User {email} created successfully!");
        var dbUser = await userManager.FindByEmailAsync(email);

        var roleResult = await userManager.AddToRoleAsync(dbUser, "Admin");
        if (roleResult.Succeeded)
        {
            Console.WriteLine($"User {email} added to Admin role successfully!");
        }
    }

    foreach (var err in result.Errors)
    {
        Console.WriteLine(err.Description);
    }
}

Main().Wait();



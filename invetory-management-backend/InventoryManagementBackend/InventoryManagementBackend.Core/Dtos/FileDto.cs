namespace InventoryManagementBackend.Core.Dtos;

public class FileDto
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Path { get; set; }
    public string Extension { get; set; }
    public string OriginalName { get; set; }
}
namespace InventoryManagementBackend.Core.Dtos;

public class ItemDto
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Quantity { get; set; }
    public string ImagePath { get; set; }
    public string LocationId { get; set; }
    public string LocationName { get; set; }
    public bool NeedsApproval { get; set; }
    public string InventoryName { get; set; }
}

public class ItemPostDto
{
    public string Name { get; set; }
    public string Description { get; set; }
    public int Quantity { get; set; }
    public string ImageId { get; set; }
    public string LocationId { get; set; }
    public string InventoryId { get; set; }
    public bool NeedsApproval { get; set; }
    public string? RequestDescription { get; set; }
}

public class ItemDeleteDto
{
    public string Id { get; set; }
    public bool NeedsApproval { get; set; }
    public string? RequestDescription { get; set; }
}

public class ItemTransferDto
{
    public string Id { get; set; }
    public string NewLocationId { get; set; }
    public string NewInventoryId { get; set; }
    public int NewQuantity { get; set; }
    public string? RequestDescription { get; set; }
}
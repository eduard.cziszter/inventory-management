using InventoryManagementBackend.Api.Dtos.Abstractions;

namespace InventoryManagementBackend.Core.Dtos;

public class SettingDto : SluggableDto
{
    public string Value { get; set; }
}
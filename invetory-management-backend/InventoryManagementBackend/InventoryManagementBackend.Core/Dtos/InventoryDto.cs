using InventoryManagementBackend.Api.Dtos.Abstractions;

namespace InventoryManagementBackend.Core.Dtos;

public class InventoryDto : SluggableDto
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string OwnerId { get; set; }
    public string OwnerEmail { get; set; }
    public IEnumerable<ItemDto> Items { get; set; }
}

public class InventoryPostDto : SluggableDto
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string OrganizationId { get; set; }
}
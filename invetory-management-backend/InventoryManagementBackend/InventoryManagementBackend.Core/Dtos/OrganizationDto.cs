using InventoryManagementBackend.Api.Dtos.Abstractions;

namespace InventoryManagementBackend.Core.Dtos;

public class OrganizationDto : SluggableDto
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string ShortName { get; set; }
    public string Description { get; set; }
    public string LogoPath { get; set; }
    public string DepartmentName { get; set; }
}
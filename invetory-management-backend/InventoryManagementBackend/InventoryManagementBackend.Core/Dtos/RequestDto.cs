namespace InventoryManagementBackend.Core.Dtos;

public class RequestDto
{
    public string Id { get; set; }
    public ItemDto Item { get; set; }
    public bool NeedsApproval { get; set; }
    public ItemDto NewItem { get; set; }
    public string RequestType { get; set; }
    public string? Description { get; set; }
    public string ApproverEmail { get; set; }
}
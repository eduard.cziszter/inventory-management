using InventoryManagementBackend.Core.Entities;

namespace InventoryManagementBackend.Core.Utils;

public class OrganizationForUser
{
    public string OrgId { get; set; }
    public string UserId { get; set; }
    public string ShortName { get; set; }
    public bool IsAssigned { get; set; }
    public IEnumerable<Department> Departments { get; set; }
    public Department? SelectedDepartment { get; set; }
}
namespace InventoryManagementBackend.Core.Utils;

public class UserInAdmin
{
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Roles { get; set; }
    public string Organizations { get; set; }
}
namespace InventoryManagementBackend.Core.Utils;

public class OrganizationDepartmentIds
{
    public string OrganizationId { get; set; }
    public string DepartmentId { get; set; }
}
namespace InventoryManagementBackend.Core.Utils;

public class RequestEmail
{
    public RequestType RequestType { get; set; }
    public string UserName { get; set; }
    public string ItemName { get; set; }
    public string InventoryName { get; set; }
}
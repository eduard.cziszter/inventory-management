namespace InventoryManagementBackend.Core.Utils;

public enum RequestType
{
    EditRequest,
    DeleteRequest,
    TransferRequest
}
namespace InventoryManagementBackend.Api.Dtos.Abstractions;

public class SluggableDto
{
    public string Slug { get; set; }
}
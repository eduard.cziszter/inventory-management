namespace InventoryManagementBackend.Core.Abstractions;

public class SluggableEntity : BaseEntity
{
    public string Slug { get; set; }
}
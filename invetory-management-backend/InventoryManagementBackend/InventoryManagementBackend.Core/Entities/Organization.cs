using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Organization : SluggableEntity
{
    public string Name { get; set; }
    public string ShortName { get; set; }
    public string Description { get; set; }
    [ForeignKey("LogoId")]
    public File Logo { get; set; }
    public string LogoId { get; set; }
}
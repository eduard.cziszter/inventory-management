using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;
using Microsoft.AspNetCore.Identity;

namespace InventoryManagementBackend.Core.Entities;

public class Permission : BaseEntity
{
    public string Name { get; set; }
    public string RoleId { get; set; }
    [ForeignKey("RoleId")]
    public IdentityRole Role { get; set; }
}
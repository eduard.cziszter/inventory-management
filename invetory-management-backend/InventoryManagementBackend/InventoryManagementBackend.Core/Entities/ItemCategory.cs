using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class ItemCategory : BaseEntity
{
    [ForeignKey("ItemId")]
    public Item Item { get; set; }
    public string ItemId { get; set; }
    [ForeignKey("CategoryId")]
    public Category Category { get; set; }
    public string CategoryId { get; set; }
}
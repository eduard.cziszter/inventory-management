using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class File : BaseEntity
{
    public string Name { get; set; }
    public string Path { get; set; }
    public string Extension { get; set; }
    public string OriginalName { get; set; }
}
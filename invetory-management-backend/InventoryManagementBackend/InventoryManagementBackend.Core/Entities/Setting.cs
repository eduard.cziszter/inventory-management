using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Setting : SluggableEntity
{
    public bool IsEnabled { get; set; }
    public string Value { get; set; }
}
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Location : BaseEntity
{
    public string Name { get; set; }
}
using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Department : BaseEntity
{
    public string Name { get; set; }
    public string Description { get; set; }
    [ForeignKey("OrganizationId")]
    public Organization Organization { get; set; }
    public string OrganizationId { get; set; }
}
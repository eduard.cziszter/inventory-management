using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Inventory : SluggableEntity
{
    public string Name { get; set; }
    public string Description { get; set; }
    [ForeignKey("OrganizationId")]
    public Organization Organization { get; set; }
    public string OrganizationId { get; set; }
    [ForeignKey("OwnerId")]
    public ApplicationUser Owner { get; set; }
    public string OwnerId { get; set; }
}
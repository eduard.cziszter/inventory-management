using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Category : SluggableEntity
{
    public string Name { get; set; }
    public string Description { get; set; }
}
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using InventoryManagementBackend.Core.Abstractions;
using InventoryManagementBackend.Core.Utils;

namespace InventoryManagementBackend.Core.Entities;

public class Request : BaseEntity
{
    [ForeignKey("UserId")]
    public ApplicationUser User { get; set; }
    public string UserId { get; set; }
    [ForeignKey("ItemId")]
    public Item Item { get; set; }
    public string ItemId { get; set; }
    public bool NeedsApproval { get; set; }
    [ForeignKey("NewItemId")]
    public Item NewItem { get; set; }
    public string? NewItemId { get; set; }
    public RequestType RequestType { get; set; }
    public string? Description { get; set; }
    [ForeignKey("ApproverId")]
    public ApplicationUser Approver { get; set; }
    public string? ApproverId { get; set; }
}
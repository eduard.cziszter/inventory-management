using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class UserOrganization : BaseEntity
{
    [ForeignKey("UserId")]
    public ApplicationUser User { get; set; }
    public string UserId { get; set; }
    [ForeignKey("OrganizationId")]
    public Organization Organization { get; set; }
    public string OrganizationId { get; set; }
    [ForeignKey("DepartmentId")]
    public Department Department { get; set; }
    public string? DepartmentId { get; set; }
}
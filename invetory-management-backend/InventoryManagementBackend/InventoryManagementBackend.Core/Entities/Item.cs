using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using InventoryManagementBackend.Core.Abstractions;

namespace InventoryManagementBackend.Core.Entities;

public class Item : BaseEntity
{
    public string Name { get; set; }
    public string Description { get; set; }
    public int Quantity { get; set; }
    [ForeignKey("ImageId")]
    public File Image { get; set; }
    public string ImageId { get; set; }
    [ForeignKey("LocationId")]
    public Location Location { get; set; }
    public string LocationId { get; set; }
    [ForeignKey("InventoryId")]
    public Inventory Inventory { get; set; }
    public string InventoryId { get; set; }
    public bool NeedsApproval { get; set; }
    public bool IsEditItem { get; set; }
}
using System.Security.Claims;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("api/requests")]
[Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class RequestsController : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IRequestsRepository _requestsRepository;
    private readonly IItemsRepository _itemsRepository;
    private readonly ICustomMapper _customMapper;
    private readonly IApproveRequestHelper _approveRequestHelper;

    public RequestsController(UserManager<ApplicationUser> userManager, IRequestsRepository requestsRepository,
        ICustomMapper customMapper, IItemsRepository itemsRepository, IApproveRequestHelper approveRequestHelper)
    {
        _userManager = userManager;
        _requestsRepository = requestsRepository;
        _customMapper = customMapper;
        _itemsRepository = itemsRepository;
        _approveRequestHelper = approveRequestHelper;
    }
    
    [HttpGet("get-own")]
    public async Task<IActionResult> GetRequests()
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();

        var requests = await _requestsRepository.GetRequestsByUserId(user.Id);
        if (!requests.Any()) return NotFound();

        var requestResponse = _customMapper.MapRequests(requests);

        return Ok(requestResponse);
    }
    
    [HttpGet("get-approve")]
    public async Task<IActionResult> GetApproveRequests()
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();

        var requests = await _requestsRepository.GetRequestsByApproverId(user.Id);
        if (!requests.Any()) return NotFound();

        var requestResponse = _customMapper.MapRequests(requests);

        return Ok(requestResponse);
    }
    
    [HttpDelete("{requestId}")]
    public async Task<IActionResult> DeleteRequest(string requestId)
    {
        var request = await _requestsRepository.GetAsync(r => r.Id == requestId);
        if (request is null) return NotFound();
        
        if (request.RequestType == RequestType.EditRequest)
        {
            var newItem = await _itemsRepository.GetAsync(i => i.Id == request.NewItemId);
            if (newItem is null) return BadRequest("Invalid edit request!");
            await _itemsRepository.DeleteAsync(newItem);
        }

        await _requestsRepository.DeleteAsync(request);
        return Ok();
    }

    [HttpGet("approve/{requestId}")]
    public async Task<IActionResult> ApproveRequest(string requestId)
    {
        var request = await _requestsRepository.GetAsync(requestId);
        if (request is null) return NotFound();
        
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        if (request.NeedsApproval) return BadRequest("This request requires an admins approval!");
        
        if (request.ApproverId != user.Id) return BadRequest("You are not the approver of this request!");
        
        await _approveRequestHelper.ApproveRequestAsync(request);
        return Ok();
    }
}
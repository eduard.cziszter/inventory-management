using System.Security.Claims;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("api/organizations")]
[Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class OrganizationsController : Controller
{
    private readonly IUserOrganizationsRepository _userOrganizationsRepository;
    private readonly IOrganizationsRepository _organizationsRepository;
    private readonly ICustomMapper _customMapper;
    private readonly UserManager<ApplicationUser> _userManager;

    public OrganizationsController(IUserOrganizationsRepository userOrganizationsRepository, ICustomMapper customMapper,
        UserManager<ApplicationUser> userManager, IOrganizationsRepository organizationsRepository)
    {
        _userOrganizationsRepository = userOrganizationsRepository;
        _customMapper = customMapper;
        _userManager = userManager;
        _organizationsRepository = organizationsRepository;
    }
    
    [HttpGet("get-own")]
    public async Task<IActionResult> GetOrganizations()
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        var userOrganizations = await _userOrganizationsRepository.GetOrganizationsForUserAsync(user.Id);
        if (!userOrganizations.Any()) return NotFound();
        
        return Ok(_customMapper.OrganizationsFromUserOrganizations(userOrganizations));
    }

    [HttpGet("{slug}")]
    public async Task<IActionResult> GetBySlug(string slug)
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        var userOrganization = await _userOrganizationsRepository
            .GetAsync(uo => uo.UserId == user.Id && uo.Organization.Slug == slug);

        if (userOrganization is null) return BadRequest("User not member of organization");
        
        var organization = await _organizationsRepository.GetAsync(o => o.Slug == slug);
        if (organization is null) return NotFound();

        return Ok(_customMapper.MapOrganization(organization));
    }
}
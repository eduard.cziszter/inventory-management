using System.Security.Claims;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("api/users")]
[Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class UsersController : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly ICustomMapper _customMapper;

    public UsersController(UserManager<ApplicationUser> userManager, ICustomMapper customMapper)
    {
        _userManager = userManager;
        _customMapper = customMapper;
    }
    
    [HttpGet("get-own")]
    public async Task<IActionResult> GetOrganizations()
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();

        return Ok(_customMapper.MapApplicationUser(user));
    }
}
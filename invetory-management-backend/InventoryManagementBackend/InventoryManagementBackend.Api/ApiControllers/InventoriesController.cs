using System.Security.Claims;
using InventoryManagementBackend.Core.Dtos;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("api/inventories")]
[Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class InventoriesController : Controller
{
    private readonly IInventoriesRepository _inventoryRepository;
    private readonly IItemsRepository _itemsRepository;
    private readonly ICustomMapper _customMapper;
    private readonly UserManager<ApplicationUser> _userManager;

    public InventoriesController(IInventoriesRepository inventoryRepository, ICustomMapper customMapper,
        IItemsRepository itemsRepository, UserManager<ApplicationUser> userManager)
    {
        _inventoryRepository = inventoryRepository;
        _customMapper = customMapper;
        _itemsRepository = itemsRepository;
        _userManager = userManager;
    }
    
    [HttpGet("get-by-organization")]
    public async Task<IActionResult> GetInventoriesByOrganization([FromQuery] string orgSlug)
    {
        var inventories = await _inventoryRepository.GetByOrganizationSlug(orgSlug);
        if (!inventories.Any()) return NotFound();
        
        var responseInventories = new List<InventoryDto>();

        foreach (var inventory in inventories)
        {
            var items = await _itemsRepository.GetByInventoryIdAsync(inventory.Id);
            responseInventories.Add(_customMapper.MapInventory(inventory, items));
        }
        
        return Ok(responseInventories);
    }
    
    [HttpPost]
    public async Task<IActionResult> CreateInventory([FromBody] InventoryPostDto inventoryDto)
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        if (!_validInventory(inventoryDto)) return BadRequest("Invalid inventory!");

        var inventory = _customMapper.InventoryFromDto(inventoryDto);
        inventory.OwnerId = user.Id;
        
        var addedInventory = await _inventoryRepository.AddAsync(inventory);
        var inventoryResponse = _customMapper.MapInventory(addedInventory, new List<Item>());

        return Created(string.Empty, inventoryResponse);
    }
    
    [HttpDelete("{inventoryId}")]
    public async Task<IActionResult> DeleteRequest(string inventoryId)
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        var inventory = await _inventoryRepository.GetAsync(inventoryId);
        
        if (inventory is null) return NotFound();
        
        if (inventory.OwnerId != user.Id) return BadRequest("You are not the owner of this inventory!");

        await _inventoryRepository.DeleteAsync(inventory);
        return Ok();
    }
    
    private bool _validInventory(InventoryPostDto inventory)
    {
        return !inventory.Name.IsNullOrEmpty() &&
               !inventory.Description.IsNullOrEmpty() &&
               !inventory.Slug.IsNullOrEmpty() &&
               !inventory.OrganizationId.IsNullOrEmpty();
    }
}
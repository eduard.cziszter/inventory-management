using InventoryManagementBackend.Core.Abstractions;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("/api/settings")]
public class SettingsController : Controller
{
    private readonly ISettingsRepository _settingsRepository;
    private readonly ICustomMapper _customMapper;

    public SettingsController(ISettingsRepository settingsRepository, ICustomMapper customMapper)
    {
        _settingsRepository = settingsRepository;
        _customMapper = customMapper;
    }
    
    [HttpGet("slug")]
    public async Task<IActionResult> GetSettingBySlug(string slug)
    {
        var result = await _settingsRepository.GetAsync(s => s.Slug == slug);
        if (result is null) return NotFound();
        if (!result.IsEnabled) return BadRequest("Setting is disabled!");
        return Ok(_customMapper.MapSetting(result));
    }
}
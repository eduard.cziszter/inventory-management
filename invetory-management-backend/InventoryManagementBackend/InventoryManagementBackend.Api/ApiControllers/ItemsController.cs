using System.Security.Claims;
using InventoryManagementBackend.Core.Dtos;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("api/items")]
[Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class ItemsController : Controller
{
    private readonly IItemsRepository _itemsRepository;
    private readonly ICustomMapper _customMapper;
    private readonly IFilesRepository _filesRepository;
    private readonly ILocationsRepository _locationsRepository;
    private readonly IRequestsRepository _requestsRepository;
    private readonly IInventoriesRepository _inventoriesRepository;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IEmailSender _emailSender;

    public ItemsController(IItemsRepository itemsRepository, ICustomMapper customMapper,
        IFilesRepository filesRepository, ILocationsRepository locationsRepository,
        UserManager<ApplicationUser> userManager, IRequestsRepository requestsRepository,
        IEmailSender emailSender, IInventoriesRepository inventoriesRepository)
    {
        _itemsRepository = itemsRepository;
        _customMapper = customMapper;
        _filesRepository = filesRepository;
        _locationsRepository = locationsRepository;
        _userManager = userManager;
        _requestsRepository = requestsRepository;
        _emailSender = emailSender;
        _inventoriesRepository = inventoriesRepository;
    }
    
    [HttpGet("get-image/{itemId}")]
    public async Task<IActionResult> GetItemImage(string itemId)
    {
        var item = await _itemsRepository.GetAsync(i => i.Id == itemId);
        if (item is null) return BadRequest("Invalid item id!");

        var imageResponse = _customMapper.MapFile(item.Image);

        return Ok(imageResponse);
    }
    
    [HttpGet("get-by-inventory")]
    public async Task<IActionResult> GetItemsByInventory([FromQuery] string inventoryId)
    {
        var items = await _itemsRepository.GetByInventoryIdAsync(inventoryId);
        if (!items.Any()) return NotFound();

        var responseItems = _customMapper.MapItems(items);

        return Ok(responseItems);
    }
    
    [HttpPost]
    public async Task<IActionResult> CreateItem([FromBody] ItemPostDto itemDto)
    {
        if (!_validItem(itemDto)) return BadRequest("Invalid inventory!");

        var item = _customMapper.ItemFromDto(itemDto);
        
        var addedItem = await _itemsRepository.AddAsync(item);
        var location = await _locationsRepository.GetAsync(l => l.Id == addedItem.LocationId);
        var image = await _filesRepository.GetAsync(f => f.Id == addedItem.ImageId);
        var inventory = await _inventoriesRepository.GetAsync(i => i.Id == addedItem.InventoryId);
        addedItem.Location = location;
        addedItem.Image = image;
        addedItem.Inventory = inventory;
        
        var inventoryResponse = _customMapper.MapItem(addedItem);

        return Created(string.Empty, inventoryResponse);
    }

    [HttpPost("edit/{itemId}")]
    public async Task<IActionResult> EditItem([FromBody] ItemPostDto itemDto, string itemId)
    {
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        if (!_validItem(itemDto)) return BadRequest("Invalid inventory!");

        var dbItem = await _itemsRepository.GetAsync(itemId);
        if (dbItem is null) return BadRequest("Invalid item sent!");
        
        var item = _customMapper.ItemFromDto(itemDto);

        if (!item.NeedsApproval && dbItem.Inventory.OwnerId == user.Id)
        {
            await _itemsRepository.UpdateAsync(itemId, item);
            return Ok(false);
        }

        item.IsEditItem = true;
        var addedEditItem = await _itemsRepository.AddAsync(item);
        
        var editRequest = new Request
        {
            UserId = user.Id,
            ItemId = itemId,
            NewItemId = addedEditItem.Id,
            RequestType = RequestType.EditRequest,
            NeedsApproval = item.NeedsApproval,
            Description = itemDto.RequestDescription ?? string.Empty,
        };

        if (!item.NeedsApproval)
        {
            editRequest.ApproverId = dbItem.Inventory.OwnerId;
        }

        await _requestsRepository.AddAsync(editRequest);

        if (!item.NeedsApproval)
        {
            var approverUser = await _userManager.FindByIdAsync(dbItem.Inventory.OwnerId);
            var requestEmail = new RequestEmail
            {
                RequestType = RequestType.EditRequest,
                UserName = user.UserName,
                ItemName = dbItem.Name,
                InventoryName = dbItem.Inventory.Name,
            };
            
            await _emailSender.SendEmailAsync(approverUser.Email, "Edit Request", requestEmail);
        }
        
        return Ok(true);
    }

    [HttpDelete("delete-many")]
    public async Task<IActionResult> DeleteItems([FromBody] List<ItemDeleteDto> items)
    {
        if (!items.Any()) return BadRequest("No items to delete!");

        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();

        var redirectToRequests = false;
        var deleteIds = new List<string>();

        foreach (var item in items)
        {
            var dbItem = await _itemsRepository.GetAsync(item.Id);
            if (dbItem is null) return BadRequest("Invalid item sent!");

            if (!item.NeedsApproval && dbItem.Inventory.OwnerId == user.Id)
            {
                deleteIds.Add(item.Id);
                continue;
            }

            var deleteRequest = new Request
            {
                UserId = user.Id,
                ItemId = item.Id,
                RequestType = RequestType.DeleteRequest,
                NeedsApproval = item.NeedsApproval,
                Description = item.RequestDescription ?? string.Empty,
            };

            if (!item.NeedsApproval)
            {
                deleteRequest.ApproverId = dbItem.Inventory.OwnerId;
            }

            await _requestsRepository.AddAsync(deleteRequest);

            if (!item.NeedsApproval)
            {
                var approverUser = await _userManager.FindByIdAsync(dbItem.Inventory.OwnerId);
                var requestEmail = new RequestEmail
                {
                    RequestType = RequestType.DeleteRequest,
                    UserName = user.UserName,
                    ItemName = dbItem.Name,
                    InventoryName = dbItem.Inventory.Name,
                };
            
                await _emailSender.SendEmailAsync(approverUser.Email, "Edit Request", requestEmail);
            }
            
            redirectToRequests = true;
        }

        await _itemsRepository.DeleteAsync(deleteIds);

        return Ok(redirectToRequests);
    }

    [HttpPost("transfer-many")]
    public async Task<IActionResult> TransferItem([FromBody] List<ItemTransferDto> items)
    {
        if (!items.Any()) return BadRequest("No items to delete!");
        
        var userName = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        if (userName is null) return BadRequest();
        var user = await _userManager.FindByNameAsync(userName);
        if (user is null) return BadRequest();
        
        var redirectToRequests = false;

        foreach (var item in items)
        {
            var dbItem = await _itemsRepository.GetAsync(item.Id);
            if (dbItem is null) return BadRequest("Invalid item sent!");
            
            if (dbItem.Quantity < item.NewQuantity)
            {
                return BadRequest("Not enough items to transfer!");
            }
            
            var newItem = new Item
            {
                Name = dbItem.Name,
                Description = dbItem.Description,
                Quantity = item.NewQuantity,
                ImageId = dbItem.ImageId,
                LocationId = item.NewLocationId,
                InventoryId = item.NewInventoryId,
                NeedsApproval = dbItem.NeedsApproval,
            };


            if (!dbItem.NeedsApproval && dbItem.Inventory.OwnerId == user.Id)
            {
                dbItem.Quantity -= newItem.Quantity;
                if (dbItem.Quantity > 0) await _itemsRepository.UpdateAsync(item.Id, dbItem);
                else await _itemsRepository.DeleteAsync(item.Id);
                if (newItem.Quantity > 0) await _itemsRepository.AddAsync(newItem);
                continue;
            }

            newItem.IsEditItem = true;
            var addedNewItem = await _itemsRepository.AddAsync(newItem);

            var transferRequest = new Request
            {
                UserId = user.Id,
                ItemId = item.Id,
                NewItemId = addedNewItem.Id,
                RequestType = RequestType.TransferRequest,
                NeedsApproval = dbItem.NeedsApproval,
                Description = item.RequestDescription ?? string.Empty,
            };

            if (!dbItem.NeedsApproval)
            {
                transferRequest.ApproverId = dbItem.Inventory.OwnerId;
            }
            
            await _requestsRepository.AddAsync(transferRequest);
            
            if (!dbItem.NeedsApproval)
            {
                var approverUser = await _userManager.FindByIdAsync(dbItem.Inventory.OwnerId);
                var requestEmail = new RequestEmail
                {
                    RequestType = RequestType.TransferRequest,
                    UserName = user.UserName,
                    ItemName = dbItem.Name,
                    InventoryName = dbItem.Inventory.Name,
                };
            
                await _emailSender.SendEmailAsync(approverUser.Email, "Edit Request", requestEmail);
            }
            
            redirectToRequests = true;
        }

        return Ok(redirectToRequests);
    }
    
    private bool _validItem(ItemPostDto item)
    {
        return !item.Name.IsNullOrEmpty() &&
               !item.Description.IsNullOrEmpty() &&
               !item.ImageId.IsNullOrEmpty() &&
               !item.LocationId.IsNullOrEmpty() &&
               !item.InventoryId.IsNullOrEmpty();

    }
}
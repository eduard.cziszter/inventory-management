using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("api/locations")]
[Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public class LocationsController : Controller
{
    private readonly ILocationsRepository _locationsRepository;
    
    public LocationsController(ILocationsRepository locationsRepository)
    {
        _locationsRepository = locationsRepository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var locations = await _locationsRepository.GetAsync();
        if (!locations.Any()) return NotFound();

        return Ok(locations);
    }
}
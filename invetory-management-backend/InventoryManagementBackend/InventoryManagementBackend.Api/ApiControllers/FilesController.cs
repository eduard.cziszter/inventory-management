using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Repositories.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("/api/files")]
public class FilesController : Controller
{
    private readonly IFilesRepository _filesRepository;
    private readonly ISettingsRepository _settingsRepository;
    private readonly ICustomMapper _customMapper;
    private readonly IUploadManager _uploadManager;

    public FilesController(IFilesRepository filesRepository, ISettingsRepository settingsRepository,
        ICustomMapper customMapper, IUploadManager uploadManager)
    {
        _filesRepository = filesRepository;
        _settingsRepository = settingsRepository;
        _customMapper = customMapper;
        _uploadManager = uploadManager;
    }
    
    [HttpGet("logo")]
    public async Task<IActionResult> GetLogo()
    {
        var setting = await _settingsRepository.GetAsync(s => s.Slug == "logo");
        if (setting is null) return NotFound();
        if (!setting.IsEnabled) return BadRequest("Setting is disabled!");
        var logo = await _filesRepository.GetAsync(f => f.OriginalName == setting.Value);
        if (logo is null) return NotFound();
        return Ok(_customMapper.MapFile(logo));
    }

    [HttpPost("upload")]
    [Authorize(Roles = "Member", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public async Task<IActionResult> UploadFile(IFormFile file)
    {
        var result = await _uploadManager.Upload(file);

        if (result.HasErrors()) return BadRequest(result.Errors);

        var fileResponse = _customMapper.MapFile(result.Item);
        return Ok(fileResponse);
    }
}
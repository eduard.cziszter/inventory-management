using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.ApiControllers;

[ApiController]
[Route("/api/auth")]
public class AuthController : Controller
{
    private readonly IIdentityService _identityService;

    public AuthController(IIdentityService identityService)
    {
        _identityService = identityService;
    }
    
    [HttpPost("google-login")]
    public async Task<IActionResult> GoogleSignIn()
    {
        string token = Request.Headers["Authorization"].ToString().Remove(0,7);
        
        try
        {
            var response = await _identityService.GoogleSignIn(token);
            if (response.HasErrors())
            {
                return BadRequest(response.Errors);
            }

            return Ok(((ActionResponse<Session>) response).Item);
        } catch (Exception e)
        {
            return Unauthorized();
        }
        
    }
}
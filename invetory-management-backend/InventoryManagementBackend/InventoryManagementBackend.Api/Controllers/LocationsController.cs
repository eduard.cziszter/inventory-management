using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class LocationsController : Controller
{
    private readonly ILocationsRepository _locationsRepository;

    public LocationsController(ILocationsRepository locationsRepository)
    {
        _locationsRepository = locationsRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var location = await _locationsRepository.GetAsync();
        return View(location);
    }
    
    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(Location location)
    {
        if (!_validLocation(location))
        {
            return View();
        }
        
        await _locationsRepository.AddAsync(location);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string id)
    {
        var response = await _locationsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(Location location)
    {
        if (!_validLocation(location))
            return View();

        await _locationsRepository.UpdateAsync(location.Id, location);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _locationsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _locationsRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Details(string id)
    {
        var response = await _locationsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    private bool _validLocation(Location location)
    {
        return !location.Name.IsNullOrEmpty();
    }
}
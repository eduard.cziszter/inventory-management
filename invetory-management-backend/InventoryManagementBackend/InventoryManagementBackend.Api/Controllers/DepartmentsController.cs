using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class DepartmentsController : Controller
{
    private readonly IDepartmentsRepository _departmentsRepository;
    private readonly IOrganizationsRepository _organizationsRepository;

    public DepartmentsController(IDepartmentsRepository departmentsRepository, IOrganizationsRepository organizationsRepository)
    {
        _departmentsRepository = departmentsRepository;
        _organizationsRepository = organizationsRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var departments = await _departmentsRepository.GetAsync();
        return View(departments);
    }
    
    [HttpGet]
    public async Task<IActionResult> Create()
    {
        var organizations = await _organizationsRepository.GetAsync();
        ViewBag.Organizations = organizations;
        
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(Department department)
    {
        if (!_validDepartment(department))
        {
            var organizations = await _organizationsRepository.GetAsync();
            ViewBag.Organizations = organizations;
            return View();
        }
        
        await _departmentsRepository.AddAsync(department);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string id)
    {
        var response = await _departmentsRepository.GetAsync(p => p.Id == id);
        
        var organizations = await _organizationsRepository.GetAsync();
        ViewBag.Organizations = organizations;

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(Department department)
    {
        if (!_validDepartment(department))
        {
            var organizations = await _organizationsRepository.GetAsync();
            ViewBag.Organizations = organizations;
            return View();
        }

        await _departmentsRepository.UpdateAsync(department.Id, department);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _departmentsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _departmentsRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Details(string id)
    {
        var response = await _departmentsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }

    private bool _validDepartment(Department department)
    {
        return !department.Name.IsNullOrEmpty() &&
               !department.Description.IsNullOrEmpty() &&
               !department.OrganizationId.IsNullOrEmpty();
    }
}
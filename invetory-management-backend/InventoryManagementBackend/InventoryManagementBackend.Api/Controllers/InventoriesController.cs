using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class InventoriesController : Controller
{
    private readonly IInventoriesRepository _inventoriesRepository;
    private readonly IOrganizationsRepository _organizationsRepository;
    private readonly UserManager<ApplicationUser> _userManager;

    public InventoriesController(IInventoriesRepository inventoriesRepository,
        IOrganizationsRepository organizationsRepository, UserManager<ApplicationUser> userManager)
    {
        _inventoriesRepository = inventoriesRepository;
        _organizationsRepository = organizationsRepository;
        _userManager = userManager;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var inventories = await _inventoriesRepository.GetAsync();
        return View(inventories);
    }
    
    [HttpGet]
    public async Task<IActionResult> Create()
    {
        var organizations = await _organizationsRepository.GetAsync();
        var users = await _userManager.Users.ToListAsync();
        ViewBag.Organizations = organizations;
        ViewBag.Users = users;
        
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(Inventory inventory)
    {
        if (!_validInventory(inventory))
        {
            var organizations = await _organizationsRepository.GetAsync();
            var users = await _userManager.Users.ToListAsync();
            ViewBag.Organizations = organizations;
            ViewBag.Users = users;
            return View();
        }
        
        await _inventoriesRepository.AddAsync(inventory);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string id)
    {
        var response = await _inventoriesRepository.GetAsync(p => p.Id == id);
        
        var organizations = await _organizationsRepository.GetAsync();
        var users = await _userManager.Users.ToListAsync();
        ViewBag.Organizations = organizations;
        ViewBag.Users = users;

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(Inventory inventory)
    {
        if (!_validInventory(inventory))
        {
            var organizations = await _organizationsRepository.GetAsync();
            var users = await _userManager.Users.ToListAsync();
            ViewBag.Organizations = organizations;
            ViewBag.Users = users;
            return View();
        }

        await _inventoriesRepository.UpdateAsync(inventory.Id, inventory);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _inventoriesRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _inventoriesRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Details(string id)
    {
        var response = await _inventoriesRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    private bool _validInventory(Inventory inventory)
    {
        return !inventory.Name.IsNullOrEmpty() &&
               !inventory.Description.IsNullOrEmpty() &&
               !inventory.Slug.IsNullOrEmpty() &&
               !inventory.OrganizationId.IsNullOrEmpty();
    }
}
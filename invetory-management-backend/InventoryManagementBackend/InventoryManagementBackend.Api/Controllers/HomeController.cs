using System.Security.Claims;
using InventoryManagementBackend.Core.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.Controllers;

public class HomeController : Controller
{
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly UserManager<ApplicationUser> _userManager;

    public HomeController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
    {
        _signInManager = signInManager;
        _userManager = userManager;
    }

    public IActionResult Index()
    {
        return View();
    }
    
    public async Task Login()
    {
        await HttpContext.ChallengeAsync(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties
        {
            RedirectUri = Url.Action("GoogleResponse")
        });
    }

    public async Task<IActionResult> GoogleResponse()
    {
        var result = await HttpContext.AuthenticateAsync(GoogleDefaults.AuthenticationScheme);
        
        var identity = result.Principal.Identity;
        if (identity.IsAuthenticated == false)
        {
            return View("Index");
        }
        
        var email = result.Principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        
        var user = await _userManager.FindByEmailAsync(email);

        if (user is null)
        {
            return View("Index");
        }

        var isAdmin = await _userManager.IsInRoleAsync(user, "Admin");

        if (!isAdmin)
        {
            Console.WriteLine("baaa");
            return View("Index");
        }

        Console.WriteLine("aiciiiiii");
        await _signInManager.SignInAsync(user, true);
        return RedirectToAction("Index");
    }

    public async Task<IActionResult> Logout()
    {
        await _signInManager.SignOutAsync();
        return RedirectToAction("Index");
    }
}
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class RequestsController : Controller
{
    private readonly IRequestsRepository _requestsRepository;
    private readonly IApproveRequestHelper _approveRequestHelper;

    public RequestsController(IRequestsRepository requestsRepository, IApproveRequestHelper approveRequestHelper)
    {
        _requestsRepository = requestsRepository;
        _approveRequestHelper = approveRequestHelper;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var requests = await _requestsRepository.GetAsync();
        return View(requests);
    }

    [HttpGet]
    public async Task<IActionResult> Approve(string id)
    {
        var request = await _requestsRepository.GetAsync(id);
        return View(request);
    }

    [HttpPost]
    public async Task<IActionResult> Approve(Guid id)
    {
        var request = await _requestsRepository.GetAsync(id.ToString());
        
        if (request is null) return RedirectToAction(nameof(Index));

        await _approveRequestHelper.ApproveRequestAsync(request);
        
        return RedirectToAction(nameof(Index));
    }
}
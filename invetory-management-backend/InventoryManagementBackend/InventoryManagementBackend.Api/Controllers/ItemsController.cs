using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class ItemsController : Controller
{
    private readonly IItemsRepository _itemsRepository;
    private readonly IUploadManager _uploadManager;
    private readonly ILocationsRepository _locationsRepository;
    private readonly IInventoriesRepository _inventoriesRepository;
    

    public ItemsController(IItemsRepository itemsRepository, IUploadManager uploadManager,
        ILocationsRepository locationsRepository, IInventoriesRepository inventoriesRepository)
    {
        _itemsRepository = itemsRepository;
        _uploadManager = uploadManager;
        _locationsRepository = locationsRepository;
        _inventoriesRepository = inventoriesRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var inventories = await _itemsRepository.GetAsync();
        return View(inventories);
    }
    
    [HttpGet]
    public async Task<IActionResult> Create()
    {
        var locations = await _locationsRepository.GetAsync();
        var inventories = await _inventoriesRepository.GetAsync();
        ViewBag.Locations = locations;
        ViewBag.Inventories = inventories;
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(Item item)
    {
        if (!_validItem(item))
        {
            var locations = await _locationsRepository.GetAsync();
            var inventories = await _inventoriesRepository.GetAsync();
            ViewBag.Locations = locations;
            ViewBag.Inventories = inventories;
            return View();
        }
        
        await _itemsRepository.AddAsync(item);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string id)
    {
        var response = await _itemsRepository.GetAsync(p => p.Id == id);
        
        var locations = await _locationsRepository.GetAsync();
        var inventories = await _inventoriesRepository.GetAsync();
        ViewBag.Locations = locations;
        ViewBag.Inventories = inventories;

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(Item item)
    {
        if (!_validItem(item))
        {
            var locations = await _locationsRepository.GetAsync();
            var inventories = await _inventoriesRepository.GetAsync();
            ViewBag.Locations = locations;
            ViewBag.Inventories = inventories;
            return View();
        }

        await _itemsRepository.UpdateAsync(item.Id, item);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _itemsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _itemsRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Details(string id)
    {
        var response = await _itemsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    private bool _validItem(Item item)
    {
        return !item.Name.IsNullOrEmpty() && 
               !item.Description.IsNullOrEmpty() && 
               !item.ImageId.IsNullOrEmpty() &&
               !item.LocationId.IsNullOrEmpty() &&
               !item.InventoryId.IsNullOrEmpty();
    }
}
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class SettingsController : Controller
{
    private readonly ISettingsRepository _settingsRepository;

    public SettingsController(ISettingsRepository settingsRepository)
    {
        _settingsRepository = settingsRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var settings = await _settingsRepository.GetAsync();
        return View(settings);
    }
    
    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(Setting setting)
    {
        if (!_validSetting(setting))
        {
            return View();
        }
        
        await _settingsRepository.AddAsync(setting);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string slug)
    {
        var response = await _settingsRepository.GetAsync(p => p.Slug == slug);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(Setting setting)
    {
        if (!_validSetting(setting))
            return View();

        await _settingsRepository.UpdateAsync(setting.Id, setting);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string slug)
    {
        var response = await _settingsRepository.GetAsync(p => p.Slug == slug);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _settingsRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Details(string slug)
    {
        var response = await _settingsRepository.GetAsync(p => p.Slug == slug);

        return View(response);
    }

    private bool _validSetting(Setting setting)
    {
        return !setting.Slug.IsNullOrEmpty() && !setting.Value.IsNullOrEmpty();
    }
}
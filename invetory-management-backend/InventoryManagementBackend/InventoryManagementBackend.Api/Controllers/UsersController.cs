using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Core.Utils;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using UserOrganization = InventoryManagementBackend.Core.Entities.UserOrganization;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class UsersController : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IUserOrganizationsRepository _userOrganizationsRepository;
    private readonly IOrganizationsRepository _organizationsRepository;
    private readonly IDepartmentsRepository _departmentsRepository;
    
    public UsersController(UserManager<ApplicationUser> userManager, IUserOrganizationsRepository userOrganizationsRepository,
        IOrganizationsRepository organizationsRepository, IDepartmentsRepository departmentsRepository)
    {
        _userManager = userManager;
        _userOrganizationsRepository = userOrganizationsRepository;
        _organizationsRepository = organizationsRepository;
        _departmentsRepository = departmentsRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var users = await _userManager.Users.ToListAsync();
        var appUsers = new List<UserInAdmin>();

        foreach (var user in users)
        {
            var userOrganizations = await _userOrganizationsRepository.GetOrganizationsForUserAsync(user.Id);
            var organizations = userOrganizations.Select(o => o.Organization);
            var orgNames = String.Join(" ", organizations.Select(o => o.ShortName));
            var roles = await _userManager.GetRolesAsync(user);
            var roleNames = String.Join(" ", roles);
            appUsers.Add(new UserInAdmin
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Organizations = orgNames,
                Roles = roleNames
            });
        }

        return View(appUsers);
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string id)
    {
        var response = await _userManager.FindByIdAsync(id);

        return View(response);
    }
    
    public async Task<IActionResult> Edit(ApplicationUser user)
    {
        if (!_validUser(user))
            return View();
        
        user.Updated = DateTime.Now;

        await _userManager.UpdateAsync(user);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _userManager.FindByIdAsync(id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        var user = await _userManager.FindByIdAsync(id.ToString());
        await _userManager.DeleteAsync(user);

        return RedirectToAction(nameof(Index));
    }

    [HttpGet]
    public async Task<IActionResult> Organizations(string id)
    {
        var organizations = await _organizationsRepository.GetAsync();
        var organizationsForUser = new List<OrganizationForUser>();

        var allUserOrganizations = await _userOrganizationsRepository.GetAsync();
        var userOrganizationsIds = allUserOrganizations
            .Where(uo => uo.UserId == id)
            .Select(uo => new OrganizationDepartmentIds
            {
                DepartmentId = uo.DepartmentId ?? "",
                OrganizationId = uo.OrganizationId
            })
            .ToList();
        
        foreach (var organization in organizations)
        {
            var orgDepartments = await _departmentsRepository.GetForOrgIdAsync(organization.Id);
            
            var depId = "";
            var isAssigned = false;
            if (userOrganizationsIds.Any())
            {
                depId = userOrganizationsIds
                    .FirstOrDefault(uo => uo.OrganizationId == organization.Id)?.DepartmentId ?? "";
                isAssigned = userOrganizationsIds
                    .Select(uo => uo.OrganizationId).Contains(organization.Id);
            }
            
            organizationsForUser.Add(new OrganizationForUser
            {
                OrgId = organization.Id,
                UserId = id,
                ShortName = organization.ShortName,
                IsAssigned = isAssigned,
                SelectedDepartment = orgDepartments.FirstOrDefault(d => d.Id == depId),
                Departments = orgDepartments,
            });
        }

        return View(organizationsForUser);
    }
    
    [HttpPost]
    public async Task<IActionResult> AssignOrganization(string orgId, string departmentId, string userId)
    {
        if (orgId.IsNullOrEmpty() || userId.IsNullOrEmpty() || departmentId.IsNullOrEmpty())
        {
            return RedirectToAction(nameof(Index));
        }
        
        var userOrganization = new UserOrganization
        {
            UserId = userId,
            OrganizationId = orgId,
            DepartmentId = departmentId
        };

        await _userOrganizationsRepository.AddAsync(userOrganization);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpPost]
    public async Task<IActionResult> UnAssignOrganization(string orgId, string userId)
    {
        var userOrganization = await _userOrganizationsRepository
            .GetAsync(uo => uo.UserId == userId && uo.OrganizationId == orgId);
        
        if (userOrganization is null) return RedirectToAction(nameof(Index));
        
        await _userOrganizationsRepository.DeleteAsync(userOrganization);
        return RedirectToAction(nameof(Index));
    }
    
    private bool _validUser(ApplicationUser user)
    {
        return !user.FirstName.IsNullOrEmpty() &&
               !user.LastName.IsNullOrEmpty() &&
               !user.Email.IsNullOrEmpty();
    }
}
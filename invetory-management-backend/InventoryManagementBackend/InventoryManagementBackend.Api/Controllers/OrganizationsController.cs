using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class OrganizationsController : Controller
{
    private readonly IOrganizationsRepository _organizationsRepository;

    public OrganizationsController(IOrganizationsRepository organizationsRepository)
    {
        _organizationsRepository = organizationsRepository;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var inventories = await _organizationsRepository.GetAsync();
        return View(inventories);
    }
    
    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(Organization organization)
    {
        if (!_validOrganization(organization))
        {
            return View();
        }
        
        await _organizationsRepository.AddAsync(organization);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(string id)
    {
        var response = await _organizationsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Edit(Organization organization)
    {
        if (!_validOrganization(organization))
            return View();

        await _organizationsRepository.UpdateAsync(organization.Id, organization);

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _organizationsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _organizationsRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    [HttpGet]
    public async Task<IActionResult> Details(string id)
    {
        var response = await _organizationsRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    private bool _validOrganization(Organization organization)
    {
        return !organization.Name.IsNullOrEmpty() &&
               !organization.ShortName.IsNullOrEmpty() &&
               !organization.Description.IsNullOrEmpty() &&
               !organization.Slug.IsNullOrEmpty() &&
               !organization.LogoId.IsNullOrEmpty();
    }
}
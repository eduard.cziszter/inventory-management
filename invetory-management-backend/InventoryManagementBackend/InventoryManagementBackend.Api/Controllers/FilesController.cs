using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementBackend.Api.Controllers;

[Authorize(Roles = "Admin")]
public class FilesController : Controller
{
    private readonly IUploadManager _uploadManager;
    private readonly IFilesRepository _filesRepository;

    public FilesController(IUploadManager uploadManager, IFilesRepository filesRepository)
    {
        _uploadManager = uploadManager;
        _filesRepository = filesRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var response = await _filesRepository.GetAsync();

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Upload(IFormFile file)
    {
        if (!ModelState.IsValid) return RedirectToAction("UploadFileModal");

        var response = await _uploadManager.Upload(file);

        if (response.HasErrors()) ModelState.AddModelError("", response.GetErrors());

        return Ok(response.Item);
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(string id)
    {
        var response = await _filesRepository.GetAsync(p => p.Id == id);

        return View(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _filesRepository.DeleteAsync(id.ToString());

        return RedirectToAction(nameof(Index));
    }
    
    public async Task<IActionResult> FileSelectModal()
    {
        var files = await _filesRepository.GetAsync();
        return PartialView("_FileSelectModal", files);
    }

    public IActionResult UploadFileModal()
    {
        return PartialView("_UploadFileModal");
    }
}
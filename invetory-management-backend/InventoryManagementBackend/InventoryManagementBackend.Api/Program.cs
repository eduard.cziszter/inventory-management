using System.Text;
using InventoryManagementBackend.Core.Entities;
using InventoryManagementBackend.Infrastructure.Database;
using InventoryManagementBackend.Infrastructure.Database.Abstractions;
using InventoryManagementBackend.Infrastructure.Database.Repos;
using InventoryManagementBackend.Infrastructure.Repositories.Abstractions;
using InventoryManagementBackend.Infrastructure.Services;
using InventoryManagementBackend.Infrastructure.Services.Abstractions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "Inventory Management", Version = "V1'"} );
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter into field the JWT token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});
services.AddControllersWithViews();
services.AddRazorPages();

services.AddCors(options =>
{
    options.AddPolicy("InventoryManagementCorsPolicy", builder =>
    {
        builder
            .WithOrigins("http://localhost:4200")
            .AllowAnyMethod()
            .AllowAnyHeader();
    });
});

services.AddDefaultIdentity<ApplicationUser>()
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<AppDbContext>()
    .AddDefaultTokenProviders();

services.AddAuthentication()
    .AddCookie(options =>
    {
        options.Cookie.SameSite = SameSiteMode.None;
        options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    })
    .AddGoogle(googleOptions =>
    {
        googleOptions.ClientId = configuration["Authentication:Google:ClientId"];
        googleOptions.ClientSecret = configuration["Authentication:Google:ClientSecret"];
        googleOptions.ClaimActions.MapJsonKey("urn:google:picture", "picture", "url");
        googleOptions.CorrelationCookie.SecurePolicy = CookieSecurePolicy.Always;
        googleOptions.CorrelationCookie.SameSite = SameSiteMode.None;
    })
    .AddJwtBearer(x =>
    {
        x.SaveToken = true;
        x.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey =
                new SymmetricSecurityKey(
                    Encoding.ASCII.GetBytes(configuration["Authentication:JWT:Secret"])),
            ValidateIssuer = true,
            ValidIssuer = configuration["Authentication:JWT:Issuer"],
            ValidateAudience = false,
            RequireExpirationTime = false,
            ValidateLifetime = true
        }; 
    });

services.Configure<IdentityOptions>(options =>
{
    options.Password.RequireDigit = true;
    options.Password.RequireLowercase = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequireUppercase = false;
    options.Password.RequiredLength = 6;
    options.Password.RequiredUniqueChars = 3;
    options.SignIn.RequireConfirmedEmail = true;
});

var connectionString = configuration.GetConnectionString("Default");

services.AddDbContext<AppDbContext>(options => {
    if (builder.Environment.IsDevelopment())
    {
        options.EnableSensitiveDataLogging();
    }
    options.UseNpgsql(connectionString, x =>
    {
        x.MigrationsAssembly("InventoryManagementBackend.Infrastructure");
    });
});

services.AddScoped<ICategoriesRepository, CategoriesRepository>();
services.AddScoped<IDepartmentsRepository, DepartmentsRepository>();
services.AddScoped<IRequestsRepository, RequestRepository>();
services.AddScoped<IInventoriesRepository, InventoriesRepository>();
services.AddScoped<IItemCategoriesRepository, ItemCategoriesRepository>();
services.AddScoped<IItemsRepository, ItemsRepository>();
services.AddScoped<ILocationsRepository, LocationsRepository>();
services.AddScoped<IOrganizationsRepository, OrganizationsRepository>();
services.AddScoped<IPermissionsRepository, PermissionsRepository>();
services.AddScoped<ISettingsRepository, SettingsRepository>();
services.AddScoped<IFilesRepository, FilesRepository>();
services.AddScoped<IUserOrganizationsRepository, UserOrganizationRepository>();

services.AddScoped<IIdentityService, IdentityService>();
services.AddScoped<IUploadManager, UploadManager>();
services.AddScoped<ICustomMapper, CustomMapper>();
services.AddScoped<IApproveRequestHelper, ApproveRequestHelper>();
services.AddScoped<IEmailSender, EmailSender>();

var app = builder.Build();

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
    
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "InventoryManagementBackend.Api v1"));

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
    Console.WriteLine("Running EF migrations...");
    dbContext.Database.Migrate();
}

var contentDir = configuration["Content_Directory"];
contentDir = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), contentDir));
if (!Directory.Exists(contentDir))
{
    Console.WriteLine("Creating CONTENT_DIRECTORY: " + contentDir);
    Directory.CreateDirectory(contentDir);
}

app.UseStaticFiles();
app.UseStaticFiles(new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(contentDir),
    RequestPath = "/content",
    ServeUnknownFileTypes = true
});
app.UseHttpsRedirection();

app.UseCookiePolicy(new CookiePolicyOptions
{
    MinimumSameSitePolicy = SameSiteMode.None,
    Secure = CookieSecurePolicy.Always
});

app.UseRouting();
app.UseCors("InventoryManagementCorsPolicy");

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "admin/{controller=Home}/{action=Index}/{id?}");
    endpoints.MapRazorPages();
});

app.Run();
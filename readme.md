# Inventory Management Software System

## Prerequisites

### For Database
- PostgreSQL database server running on local machine (or remote).

### For Backend
- .NET 8 SDK - https://dotnet.microsoft.com/en-us/download/dotnet/8.0
- .NET EF Tools - run ```dotnet tool install --global dotnet-ef```.

### For Frontend
- Node.js - https://nodejs.org/en/download/
- Angular CLI - run ```npm install -g @angular/cli```.

## Backend Setup
Add the following settings in the appsettings.Development.json file located in InventoryManagementBackend.Api project:
- Change the connection string to use you local PostGreSQL server and database;
- Add a Google ClientId and ClientSecret for Google Authentication;
- Add the password for the Gmail account used for sending emails;
- Add JWT secret used for generating unique JWT tokens.

In the solution directory, InventoryManagementBackend run ```dotnet ef database update --project InventoryManagementBackend.Infrastructure/InventoryManagementBackend.Infrastructure.csproj --startup-project InventoryManagementBackend.Api/InventoryManagementBackend.Api.csproj``` to apply migrations to your database.

To run the application, use the command ```dotnet watch run --project InventoryManagementBackend.Api/InventoryManagementBackend.Api.csproj```.

To create add administrator accounts run the `Utils` project form the InventoryManagement.Utils directory using the command ```dotnet watch run```.

## Frontend Setup
To run the frontend application go to the frontend directory and run the following command:
- ```npm install``` to install all the dependencies;
- ```npm run local``` to run the frontend using a proxy to the local backend server.

To build the frontend application run the following command ```npm run build```. The build result will be stored in the `dist/` directory.
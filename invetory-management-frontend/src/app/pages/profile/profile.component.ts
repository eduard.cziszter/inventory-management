import {Component, HostListener, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';
import {IUser} from '../../models/user.interface';
import {OrganizationsService} from '../../services/organizations.service';
import {IOrganization} from '../../models/organization.interface';
import {firstValueFrom} from 'rxjs';
import {MessageService} from 'primeng/api';
import {isPlatformBrowser} from '@angular/common';
import {constants} from '../../shared/utils/constants';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.scss'
})
export class ProfileComponent implements OnInit {
  profilePicture: string | undefined;
  user!: IUser;
  organizations!: IOrganization[];
  loading = true;
  siteUrl = 'inventar.edicz.ro';
  screenWidth!: number;

  constructor(
    private readonly  _authService: AuthService,
    private readonly _userService: UserService,
    private readonly _organizationsService: OrganizationsService,
    private readonly _messageService: MessageService,
    @Inject(PLATFORM_ID) private readonly _platformId: Object
  ) {
    if (isPlatformBrowser(_platformId)) {
      this.screenWidth = window.innerWidth;
    }
  }

  async ngOnInit(): Promise<void> {
    this.loading = true;
    try {
      this.profilePicture = (await this._authService.getSession()).profilePicture.replace('=s96-c', '=s350-c');
      this.user = await this._userService.getOwn();
      this.organizations = await firstValueFrom(this._organizationsService.getOwn());
    } catch (error: any) {
      this._messageService.add({severity: 'error',
        summary: 'Ceva nu a funcționat!' + (error.status === 404 ? ' Resursele nu au fost găsite!' : '')});
    }
    this.loading = false;
  }

  get firstCardStyle() {
    return {
      backgroundImage: 'linear-gradient(65deg, #004D4D, #f0fef7)'
    }
  }

  @HostListener('window:resize', ['$event'])
  getScreenWidth(): void {
    if (isPlatformBrowser(this._platformId)) {
      this.screenWidth = window.innerWidth;
    }
  }
}

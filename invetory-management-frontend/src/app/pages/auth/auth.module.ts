import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {reverseAuthGuard} from '../../guards/reverse-auth.guard';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {SharedModule} from '../../shared/shared.module';
import {MessageService} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {ToastModule} from 'primeng/toast';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    canActivate: [reverseAuthGuard]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ProgressSpinnerModule,
    SharedModule,
    MessagesModule,
    ToastModule
  ],
  providers: [MessageService]
})
export class AuthModule { }

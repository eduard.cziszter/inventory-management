import {Component, OnInit} from '@angular/core';
import {FilesService} from '../../../../services/files.service';
import {IFile} from '../../../../models/file.interface';
import {GoogleService} from '../../../../services/google.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent implements OnInit {
  loading = true;
  logo!: IFile;

  constructor(
    private readonly _fileService: FilesService,
    private readonly _messageService: MessageService,
    public googleService: GoogleService
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.loading = true;
    try {
      this.logo = await this._fileService.getLogo();
    } catch (e) {
      this._messageService.add({severity: 'error', summary: 'Logo Not Found!'});
    }
    this.loading = false;
  }
}

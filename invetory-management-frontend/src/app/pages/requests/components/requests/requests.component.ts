import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RequestsService} from '../../../../services/requests.service';
import {IRequest} from '../../../../models/requests.interface';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrl: './requests.component.scss'
})
export class RequestsComponent implements OnInit {
  loading = true;
  requests: IRequest[] = [];

  constructor(
    private readonly _requestsService: RequestsService,
    private readonly _confirmationService: ConfirmationService,
    private readonly _messageService: MessageService,
  ) { }

  async ngOnInit(): Promise<void> {
    await this.loadRequests();
  }

  isDeleteRequest(request: IRequest): boolean {
    return request.requestType === 'Cerere de ștergere';
  }

  deleteRequest(requestId: string): void {
    this._confirmationService.confirm({
      message: 'Dorești să ștergi cererea selectată?',
      header: 'Confirmă',
      icon: 'pi pi-exclamation-triangle',
      accept: async () => {
        await this._requestsService.deleteRequest(requestId);
        await this.loadRequests();
      }
    });
  }

  async loadRequests(): Promise<void> {
    this.loading = true;
    try {
      this.requests = await this._requestsService.getOwn();
    } catch (error: any) {
      this.requests = [];
      this._messageService.add({severity: 'error',
        summary: 'Ceva nu a funcționat!' + (error.status === 404 ? ' Resursele nu au fost găsite!' : '')});
    }
    this.loading = false;
  }
}

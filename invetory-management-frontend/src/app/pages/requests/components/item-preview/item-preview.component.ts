import {Component, Input} from '@angular/core';
import {IItem} from '../../../../models/item.interface';

@Component({
  selector: 'app-item-preview',
  templateUrl: './item-preview.component.html',
  styleUrl: './item-preview.component.scss'
})
export class ItemPreviewComponent {
  @Input() item!: IItem;
  @Input() isDelete!: boolean;
}

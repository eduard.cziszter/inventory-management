import { Component } from '@angular/core';
import {IRequest} from '../../../../models/requests.interface';
import {RequestsService} from '../../../../services/requests.service';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-requests-approve',
  templateUrl: './requests-approve.component.html',
  styleUrl: './requests-approve.component.scss'
})
export class RequestsApproveComponent {
  loading = true;
  requests: IRequest[] = [];

  constructor(
    private readonly _requestsService: RequestsService,
    private readonly _confirmationService: ConfirmationService,
    private readonly _messageService: MessageService,
  ) { }

  async ngOnInit(): Promise<void> {
    await this.loadRequests();
  }

  isDeleteRequest(request: IRequest): boolean {
    return request.requestType === 'Cerere de ștergere';
  }

  approveRequest(requestId: string): void {
    this._confirmationService.confirm({
      message: 'Dorești să aprobi cererea selectată?',
      header: 'Confirmă',
      icon: 'pi pi-exclamation-triangle',
      accept: async () => {
        await this._requestsService.approveRequest(requestId);
        await this.loadRequests();
      }
    });
  }

  async loadRequests(): Promise<void> {
    this.loading = true;
    try {
      this.requests = await this._requestsService.getApprove();
    } catch (error: any) {
      this.requests = [];
      this._messageService.add({severity: 'error',
        summary: 'Ceva nu a funcționat!' + (error.status === 404 ? ' Resursele nu au fost găsite!' : '')});
    }
    this.loading = false;
  }
}

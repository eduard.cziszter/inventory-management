import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RequestsComponent} from './components/requests/requests.component';
import {RouterModule, Routes} from '@angular/router';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {CardModule} from 'primeng/card';
import {ItemPreviewComponent} from './components/item-preview/item-preview.component';
import {SharedModule} from '../../shared/shared.module';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {authGuard} from '../../guards/auth.guard';
import {ToastModule} from 'primeng/toast';
import {RequestsApproveComponent} from './components/requests-approve/requests-approve.component';

const routes: Routes = [
  {
    path: '',
    component: RequestsComponent,
    canActivate: [authGuard]
  },
  {
    path: 'approve',
    component: RequestsApproveComponent,
    canActivate: [authGuard]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    RequestsComponent,
    ItemPreviewComponent,
    RequestsApproveComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ProgressSpinnerModule,
    CardModule,
    SharedModule,
    ConfirmDialogModule,
    ToastModule
  ],
  providers: [
    ConfirmationService,
    MessageService,
  ]
})
export class RequestsModule { }

import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IItem} from '../../../../models/item.interface';
import {ILocation} from '../../../../models/location.interface';
import {FilesService} from '../../../../services/files.service';
import {ItemsService} from '../../../../services/items.service';
import {ConfirmationService} from 'primeng/api';
import {Router} from '@angular/router';
import {IInventory} from '../../../../models/inventory.interface';


@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrl: './item-edit.component.scss'
})
export class ItemEditComponent implements OnInit, OnChanges {
  @Input() item!: IItem;
  @Input() locations!: ILocation[];
  @Input() inventory!: IInventory;
  form: FormGroup;
  @Output() public editedItem: EventEmitter<boolean> = new EventEmitter<boolean>();
  webcamDialog = false;

  constructor(
    private readonly _filesService: FilesService,
    private readonly _itemsService: ItemsService,
    private readonly _confirmationService: ConfirmationService,
    private readonly _router: Router
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      quantity: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      locationId: new FormControl('', [Validators.required]),
      needsApproval: new FormControl(false),
      requestDescription: new FormControl('')
    });
  }

  async ngOnInit(): Promise<void> {
    await this.patchForm();
  }

  showWebcam(): void {
    this.webcamDialog = true;
  }

  async ngOnChanges(): Promise<void> {
    await this.patchForm();
  }

  async patchForm() {
    this.form.patchValue(this.item);
    const image = await this._itemsService.getItemImage(this.item.id);
    this.form.controls['image'].patchValue(image);
    this.form.controls['locationId'].patchValue(
      this.item.locationId
    );
  }

  async fileInputChange(e: any, controlName: string) {
    const {files} = e.target;
    if (files.length !== 1) {
      return;
    }

    const file: File = files[0];
    const response = await this._filesService.upload(file);
    this.form.controls[controlName].setValue(response);
  }

  async webcamImageTaken(dataUrl: string, controlName: string): Promise<void> {
    if (!dataUrl) {
      this.webcamDialog = false;
      return;
    }

    const arr = dataUrl.split(',');
    const mime = arr[0].match(/:(.*?);/)![1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    const blob = new Blob([u8arr], {type: mime});
    const file = new File([blob], "webcam.png", {type: "image/png"});

    const response = await this._filesService.upload(file);
    this.form.controls[controlName].setValue(response);

    this.webcamDialog = false;
  }

  removeFile(controlName: string) {
    this.form.controls[controlName].reset();
  }

  async editItem(): Promise<void> {
    const formValue = this.form.value;

    this._confirmationService.confirm({
      message: (formValue.needsApproval ? 'Acest obiect necesită aprobare pentru a fi editat. Va fi creată o cerere. ' : '') +
        'Dorești să editezi obiectul?',
      header: 'Confirmă',
      icon: 'pi pi-exclamation-triangle',
      accept: async () => {
        formValue.imageId = formValue.image.id;
        formValue.inventoryId = this.inventory.id;
        delete formValue.image;
        const requestAdded = await this._itemsService.editItem(formValue, this.item.id);
        if (requestAdded) {
          await this._router.navigate(['/requests']);
        } else {
          this.editedItem.emit(true);
        }
      }
    });
  }
}

import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {IItem, IItemTransfer} from '../../../../models/item.interface';
import {ILocation} from '../../../../models/location.interface';
import {IInventory} from '../../../../models/inventory.interface';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ItemsService} from '../../../../services/items.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-item-transfer',
  templateUrl: './item-transfer.component.html',
  styleUrl: './item-transfer.component.scss'
})
export class ItemTransferComponent implements OnChanges {
  @Input() items!: IItem[];
  @Input() locations!: ILocation[];
  @Input() inventories!: IInventory[];
  @Input() inventory!: IInventory;
  form: FormGroup;
  @Output() public transferredItems: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _itemsService: ItemsService,
    private readonly _router: Router
  ) {
    this.form = this._formBuilder.group({
      formItems: this._formBuilder.array([])
    });
  }

  ngOnChanges(): void {
    this.loadForms();
  }

  get formItems(): FormArray {
    return this.form.get('formItems') as FormArray;
  }

  addForm(item: IItemTransfer): void {
    const newForm = this._formBuilder.group({
      id: [item.id, Validators.required],
      newQuantity: [item.newQuantity, Validators.required],
      newInventoryId: [item.newInventoryId, Validators.required],
      newLocationId: [item.newLocationId, Validators.required]
    });
    this.formItems.push(newForm);
  }

  loadForms(): void {
    while (this.formItems.length > 0) {
      this.formItems.removeAt(0);
    }

    this.items.forEach(item => {
      this.addForm({
        id: item.id,
        newQuantity: item.quantity,
        newInventoryId: this.inventory.id,
        newLocationId: item.locationId,
      });
    });
  }

  getFormGroup(formControl: AbstractControl): FormGroup {
    return formControl as FormGroup;
  }

  async transferItems(): Promise<void> {
    if (!this.form.valid) {
      return;
    }

    try {
      const redirectToRequests = await this._itemsService.transferItems(this.form.get('formItems')!.value);
      if (redirectToRequests) {
        await this._router.navigate(['/requests']);
      } else {
        this.transferredItems.emit(true);
      }
    } catch (e) {

    }
  }
}

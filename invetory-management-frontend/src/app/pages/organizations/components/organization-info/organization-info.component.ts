import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IOrganization} from '../../../../models/organization.interface';
import {InventoriesService} from '../../../../services/inventories.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IInventory} from '../../../../models/inventory.interface';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-organization-info',
  templateUrl: './organization-info.component.html',
  styleUrl: './organization-info.component.scss'
})
export class OrganizationInfoComponent {
  @Input() public organization!: IOrganization;
  @Input() selectedInventory!: IInventory;
  @Output() public changedInventories: EventEmitter<boolean> = new EventEmitter<boolean>();
  infoDialogVisible = false;
  inventoryDialogVisible = false;
  form: FormGroup;

  constructor(
    private readonly _inventoriesService: InventoriesService,
    private readonly _confirmationService: ConfirmationService,
    private readonly _messageService: MessageService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  showInfoDialog(): void {
    this.infoDialogVisible = true;
  }

  showInventoryDialog(): void {
    this.inventoryDialogVisible = true;
  }

  async addInventory(): Promise<void> {
    if (!this.form.valid) {
      return;
    }

    const payload = this.form.value;
    payload.organizationId = this.organization.id;

    try {
      await this._inventoriesService.addInventory(payload);
      this.form.reset();
      this.inventoryDialogVisible = false;
      this.changedInventories.emit(true);
    } catch (error) {
      this.form.reset()
      this.changedInventories.emit(false);
    }
  }

  deleteSelectedInventory(): void {
    this.inventoryDialogVisible = false;
    this.infoDialogVisible = false;

    if (!this.selectedInventory) {
      this._messageService.add({severity: 'error', summary: 'Nu a fost selectat nici un inventar!'});
      return;
    }

    this._confirmationService.confirm({
      message: `Dorești să ștergi inventarul ${this.selectedInventory.name}?`,
      header: 'Confirmă',
      icon: 'pi pi-exclamation-triangle',
      accept: async () => {
        try {
          await this._inventoriesService.deleteInventory(this.selectedInventory.id);
          this.changedInventories.emit(true);
        } catch (error) {
          console.log(error);
          this._messageService.add({severity: 'error', summary: 'Ceva nu a funcționat!'});
          this.changedInventories.emit(false);
        }
      }
    });
  }
}

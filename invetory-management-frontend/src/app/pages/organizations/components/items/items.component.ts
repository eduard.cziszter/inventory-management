import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IItem} from '../../../../models/item.interface';
import {IInventory} from '../../../../models/inventory.interface';
import {LocationsService} from '../../../../services/locations.service';
import {ILocation} from '../../../../models/location.interface';
import {ItemsService} from '../../../../services/items.service';
import {ConfirmationService} from 'primeng/api';
import {Router} from '@angular/router';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrl: './items.component.scss'
})
export class ItemsComponent implements OnInit, OnChanges {
  @Input() inventory!: IInventory;
  @Input() inventories!: IInventory[];
  selectedItems: IItem[] | null = null;
  locations!: ILocation[];
  addDialogVisible = false;
  editDialogVisible = false;
  deleteDialogVisible = false;
  transferDialogVisible = false;
  itemToBeEdited!: IItem;
  itemsToBeDeleted!: IItem[];
  itemsToBeTransferred!: IItem[];

  constructor(
    private readonly _locationsService: LocationsService,
    private readonly _itemsService: ItemsService,
  ) { }

  async ngOnInit(): Promise<void> {
    this.locations = await this._locationsService.getAll();
  }

  async ngOnChanges(): Promise<void> {
    await this.onUpdateItems();
  }

  showAddDialog(): void {
    this.addDialogVisible = true;
  }

  showEditDialog(item: IItem): void {
    this.itemToBeEdited = item;
    this.editDialogVisible = true;
  }

  showDeleteDialog(items: IItem[]): void {
    this.itemsToBeDeleted = items;
    this.deleteDialogVisible = true;
  }

  showTransferDialog(items: IItem[]): void {
    this.itemsToBeTransferred = items;
    this.transferDialogVisible = true;
  }

  async onUpdateItems(): Promise<void> {
    try {
      this.inventory.items = await this._itemsService.getByInventoryId(this.inventory.id);
    } catch (error: any) {
      if (error.status === 404) {
        this.inventory.items = [];
      }
    }
  }

  async onItemAction(addedItem: boolean, action: string): Promise<void> {
    if (addedItem) {
      switch (action) {
        case 'add':
          this.addDialogVisible = false;
          break;
        case 'edit':
          this.editDialogVisible = false;
          break;
        case 'delete':
          this.deleteDialogVisible = false;
          break;
        case 'transfer':
          this.transferDialogVisible = false;
          break;
      }
      await this.onUpdateItems();
    }
  }
}

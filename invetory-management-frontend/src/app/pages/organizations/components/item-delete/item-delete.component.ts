import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {IItem} from '../../../../models/item.interface';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ItemsService} from '../../../../services/items.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-item-delete',
  templateUrl: './item-delete.component.html',
  styleUrl: './item-delete.component.scss'
})
export class ItemDeleteComponent implements OnChanges {
  @Input() items!: IItem[];
  hasWithApproval = false;
  form: FormGroup;
  @Output() public deletedItems: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private readonly _itemsService: ItemsService,
    private readonly _router: Router
  ) {
    this.form = new FormGroup({
      requestDescription: new FormControl('')
    });
  }

  async deleteItems(): Promise<void> {
    if (!this.form.valid) {
      return;
    }

    const selectedDelete = this.items!.map(item => {
      return {id: item.id, needsApproval: item.needsApproval,
        requestDescription: this.form.controls['requestDescription'].value};
    });

    try {
      const redirectToRequests = await this._itemsService.deleteItems(selectedDelete);
      if (redirectToRequests) {
        await this._router.navigate(['/requests']);
      } else {
        this.deletedItems.emit(true);
      }
    } catch (err) {
      this.form.reset();
      this.deletedItems.emit(false);
    }
  }

  ngOnChanges(): void {
    this.hasWithApproval = this.items.some(i => i.needsApproval);
  }
}

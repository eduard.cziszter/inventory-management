import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {InventoriesService} from '../../../../services/inventories.service';
import {firstValueFrom, map, switchMap} from 'rxjs';
import {IOrganization} from '../../../../models/organization.interface';
import {IInventory} from '../../../../models/inventory.interface';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrl: './organization.component.scss'
})
export class OrganizationComponent implements OnInit {
  organization!: IOrganization;
  inventories!: IInventory[];
  loading = true;
  selectedInventory!: IInventory;

  constructor(
    private readonly _route: ActivatedRoute,
    private readonly _inventoriesService: InventoriesService,
    private readonly _messageService: MessageService,
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.loading = true;
    try {
      this.organization = await firstValueFrom(this._route.data.pipe(map(data => data['organizationData'])));
    } catch (error) {
      this.inventories = [];
      this._messageService.add({severity: 'error', summary: 'Organizație invalidă!'});
    }
    await this.fetchInventories(true);
    this.loading = false;
  }

  async fetchInventories(addedInventory: boolean): Promise<void> {
    if (addedInventory) {
      try {
        this.inventories = await firstValueFrom(this._inventoriesService.getByOrganizationSlug(this.organization.slug));
        console.log(this.inventories);
        this.selectedInventory = this.inventories[0];
      }
      catch (error: any) {
        this.inventories = [];
        this._messageService.add({severity: 'error',
          summary: 'Ceva nu a funcționat!' + (error.status === 404 ? ' Resursele nu au fost găsite!' : '')});
      }
    }
  }

  selectInventory(inventory: IInventory): void {
    this.selectedInventory = inventory;
  }
}

import {ResolveFn, Router} from '@angular/router';
import {inject} from '@angular/core';
import {OrganizationsService} from '../../../../services/organizations.service';
import {IOrganization} from '../../../../models/organization.interface';

export const organizationResolver: ResolveFn<IOrganization | undefined> = async (route) => {
  const organizationsService = inject(OrganizationsService);
  const router = inject(Router);

  try {
    return await organizationsService.getBySlug(route.paramMap.get('slug')!);
  } catch (exc) {
    await router.navigate(['/']);
    return undefined;
  }
};

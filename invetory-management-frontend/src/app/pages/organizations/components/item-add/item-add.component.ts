import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FilesService} from '../../../../services/files.service';
import {IInventory} from '../../../../models/inventory.interface';
import {ILocation} from '../../../../models/location.interface';
import {ItemsService} from '../../../../services/items.service';

@Component({
  selector: 'app-item-add',
  templateUrl: './item-add.component.html',
  styleUrl: './item-add.component.scss'
})
export class ItemAddComponent {
  @Input() inventory!: IInventory;
  @Input() locations!: ILocation[];
  @Output() public addedItem: EventEmitter<boolean> = new EventEmitter<boolean>();
  form: FormGroup;
  webcamDialog = false;

  constructor(
    private readonly _filesService: FilesService,
    private readonly _itemsService: ItemsService,
  ) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      quantity: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      locationId: new FormControl('', [Validators.required]),
      needsApproval: new FormControl(false),
    });
  }

  showWebcam(): void {
    this.webcamDialog = true;
  }

  async fileInputChange(e: any, controlName: string): Promise<void> {
    const {files} = e.target;
    if (files.length !== 1) {
      return;
    }

    const file: File = files[0];
    const response = await this._filesService.upload(file);
    this.form.controls[controlName].setValue(response);
  }

  async webcamImageTaken(dataUrl: string, controlName: string): Promise<void> {
    if (!dataUrl) {
      this.webcamDialog = false;
      return;
    }

    const arr = dataUrl.split(',');
    const mime = arr[0].match(/:(.*?);/)![1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    const blob = new Blob([u8arr], {type: mime});
    const file = new File([blob], "webcam.png", {type: "image/png"});

    const response = await this._filesService.upload(file);
    this.form.controls[controlName].setValue(response);

    this.webcamDialog = false;
  }

  removeFile(controlName: string) {
    this.form.controls[controlName].reset();
  }

  async addItem(): Promise<void> {
    if (!this.form.valid) {
      return;
    }

    const payload = this.form.value;
    payload.imageId = payload.image.id;
    payload.inventoryId = this.inventory.id;
    delete payload.image;

    try {
      await this._itemsService.addItem(payload);
      this.form.reset();
      this.form.controls['needsApproval'].setValue(false);
      this.addedItem.emit(true);
    } catch (error) {
      this.form.reset();
      this.form.controls['needsApproval'].setValue(false);
      this.addedItem.emit(false);
    }
  }
}

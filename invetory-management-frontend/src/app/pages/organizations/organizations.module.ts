import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrganizationComponent} from './components/organization/organization.component';
import {RouterModule, Routes} from '@angular/router';
import {organizationResolver} from './components/organization/organization.resolver';
import {OrganizationInfoComponent} from './components/organization-info/organization-info.component';
import {SplitterModule} from 'primeng/splitter';
import {SharedModule} from '../../shared/shared.module';
import {DialogModule} from 'primeng/dialog';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {ItemsComponent} from './components/items/items.component';
import {TableModule} from 'primeng/table';
import {ToolbarModule} from 'primeng/toolbar';
import {DropdownModule} from 'primeng/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ItemAddComponent} from './components/item-add/item-add.component';
import {ItemEditComponent} from './components/item-edit/item-edit.component';
import {authGuard} from '../../guards/auth.guard';
import {ToastModule} from 'primeng/toast';
import {ItemDeleteComponent} from './components/item-delete/item-delete.component';
import {ItemTransferComponent} from './components/item-transfer/item-transfer.component';

const routes: Routes = [
  {
    path: ':slug',
    component: OrganizationComponent,
    resolve: {organizationData: organizationResolver},
    canActivate: [authGuard],
  },
  {
    path: '**',
    redirectTo: ':slug',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    OrganizationComponent,
    OrganizationInfoComponent,
    ItemsComponent,
    ItemAddComponent,
    ItemEditComponent,
    ItemDeleteComponent,
    ItemTransferComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SplitterModule,
        SharedModule,
        DialogModule,
        ProgressSpinnerModule,
        ReactiveFormsModule,
        InputTextModule,
        TableModule,
        ToolbarModule,
        DropdownModule,
        FormsModule,
        CheckboxModule,
        ConfirmDialogModule,
        ToastModule,
    ],
  providers: [
    ConfirmationService,
    MessageService,
  ]
})
export class OrganizationsModule { }

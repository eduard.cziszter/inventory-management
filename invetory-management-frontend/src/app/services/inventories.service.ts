import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {IUser} from '../models/user.interface';
import {firstValueFrom, map, Observable, switchMap} from 'rxjs';
import {IInventory, IInventoryAdd} from '../models/inventory.interface';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InventoriesService {
  private readonly _baseUrl= environment.apiUrl + '/api/inventories';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public getByOrganizationSlug(orgSlug: string): Observable<IInventory[]> {
    const url = `${this._baseUrl}/get-by-organization?orgSlug=${orgSlug}`;
    return this._authService.session$
      .pipe(map(session => session.token))
      .pipe(switchMap(token => this._http.get<IInventory[]>(url, {headers: {Authorization: `Bearer ${token}`}})));
  }

  public async addInventory(inventory: IInventoryAdd): Promise<IInventory> {
    const options = await this._authService.getOptions(true);
    return firstValueFrom(this._http.post<IInventory>(this._baseUrl, inventory, options));
  }

  public async deleteInventory(inventoryId: string): Promise<void> {
    const url = `${this._baseUrl}/${inventoryId}`;
    const options = await this._authService.getOptions(true);
    return firstValueFrom(this._http.delete<void>(url, options));
  }
}

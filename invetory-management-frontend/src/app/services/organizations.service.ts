import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {catchError, firstValueFrom, map, Observable, of, switchMap, tap} from 'rxjs';
import {IOrganization} from '../models/organization.interface';

@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {
  private readonly _baseUrl= environment.apiUrl + '/api/organizations';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public getOwn(): Observable<IOrganization[]> {
    const url = `${this._baseUrl}/get-own`;
    return this._authService.session$
      .pipe(map(session => session.token))
      .pipe(
        switchMap(token => this._http.get<IOrganization[]>(url, {headers: {Authorization: `Bearer ${token}`}})),
        catchError(err => of([]))
      );
  }

  public async getBySlug(slug: string): Promise<IOrganization> {
    const url = `${this._baseUrl}/${slug}`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<IOrganization>(url, options));
  }
}

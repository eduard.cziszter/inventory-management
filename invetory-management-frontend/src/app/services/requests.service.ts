import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {IRequest} from '../models/requests.interface';
import {firstValueFrom} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  private readonly _baseUrl= environment.apiUrl + '/api/requests';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public async getOwn(): Promise<IRequest[]> {
    const url = `${this._baseUrl}/get-own`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<IRequest[]>(url, options));
  }

  public async getApprove(): Promise<IRequest[]> {
    const url = `${this._baseUrl}/get-approve`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<IRequest[]>(url, options));
  }

  public async deleteRequest(requestId: string): Promise<void> {
    const url = `${this._baseUrl}/${requestId}`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.delete<void>(url, options));
  }

  public async approveRequest(requestId: string): Promise<void> {
    const url = `${this._baseUrl}/approve/${requestId}`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<void>(url, options));
  }
}

import {Injectable, NgZone} from '@angular/core';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import { accounts } from 'google-one-tap';
import {MessageService} from 'primeng/api';
declare var google: any;

@Injectable({
  providedIn: 'root',
})
export class GoogleService {
  constructor(
    private readonly _authService: AuthService,
    private readonly _ngZone: NgZone,
    private readonly _router: Router,
    private readonly _messageService: MessageService
  ) { }

  async googleLogin(): Promise<void> {
    const gAccounts: accounts = google.accounts;

    gAccounts.id.initialize({
      client_id: '470086289827-dvbg0cqah8nqletjd9ctpcb27rsukepe.apps.googleusercontent.com',
      ux_mode: 'popup',
      use_fedcm_for_prompt: false,
      cancel_on_tap_outside: true,
      callback: ({ credential }) => {
        this._ngZone.run(async () => {
          try {
            await this._authService.loginWithGoogle(credential);
            await this._router.navigate(['/profile'])
          }
          catch (e: any) {
            this._messageService.add({severity: 'error', summary: e.message});
          }
        });
      }
    });

    gAccounts.id.prompt();
  }
}

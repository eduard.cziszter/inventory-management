import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {IFile} from '../models/file.interface';
import {firstValueFrom} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class FilesService {
  private readonly _baseUrl= environment.apiUrl + '/api/files';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public async getLogo(): Promise<IFile> {
    const url = `${this._baseUrl}/logo/`;
    return firstValueFrom(this._http.get<IFile>(url));
  }

  public async upload(file: File): Promise<IFile> {
    const url = `${this._baseUrl}/upload`;
    const options = await this._authService.getOptions(true);
    const formData = new FormData();
    formData.append('file', file, file.name);
    return firstValueFrom(this._http.post<IFile>(url, formData, options));
  }
}

import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {IUser} from '../models/user.interface';
import {firstValueFrom} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly _baseUrl= environment.apiUrl + '/api/users';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public async getOwn(): Promise<IUser> {
    const url = `${this._baseUrl}/get-own`;
    const options = await this._authService.getOptions(true);
    return firstValueFrom(this._http.get<IUser>(url, options));
  }
}

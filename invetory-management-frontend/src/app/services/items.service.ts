import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';
import {IItem, IItemAdd, IItemDelete, IItemTransfer} from '../models/item.interface';
import {firstValueFrom} from 'rxjs';
import {IFile} from '../models/file.interface';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  private readonly _baseUrl= environment.apiUrl + '/api/items';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public async getByInventoryId(inventoryId: string): Promise<IItem[]> {
    const url = `${this._baseUrl}/get-by-inventory?inventoryId=${inventoryId}`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<IItem[]>(url, options));
  }

  public async addItem(item: IItemAdd): Promise<IItem> {
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.post<IItem>(this._baseUrl, item, options));
  }

  public async deleteItems(items: IItemDelete[]): Promise<boolean> {
    const url = `${this._baseUrl}/delete-many`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.delete<boolean>(url, {body: items, ...options}));
  }

  public async getItemImage(itemId: string): Promise<IFile> {
    const url = `${this._baseUrl}/get-image/${itemId}`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<IFile>(url, options));
  }

  public async editItem(item: IItemAdd, itemId: string): Promise<boolean> {
    const url = `${this._baseUrl}/edit/${itemId}`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.post<boolean>(url, item, options));
  }

  public async transferItems(items: IItemTransfer[]): Promise<boolean> {
    const url = `${this._baseUrl}/transfer-many`;
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.post<boolean>(url, items, options));
  }
}

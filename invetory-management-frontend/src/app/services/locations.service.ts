import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {ILocation} from '../models/location.interface';
import {firstValueFrom} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  private readonly _baseUrl= environment.apiUrl + '/api/locations';

  constructor(
    private readonly _http: HttpClient,
    private readonly _authService: AuthService,
  ) { }

  public async getAll(): Promise<ILocation[]> {
    const options = await this._authService.getOptions(true);
    return await firstValueFrom(this._http.get<ILocation[]>(this._baseUrl, options));
  }
}

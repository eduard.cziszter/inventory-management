import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageMap} from '@ngx-pwa/local-storage';
import {Router} from '@angular/router';
import {ISession} from '../models/session.interface';
import {BehaviorSubject, firstValueFrom, map, Observable, tap} from 'rxjs';
import {environment} from '../../environments/environment';

export const tokenStorageKey: string = 'token';
export const sessionStorageKey: string = 'session';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token$ = new BehaviorSubject<string | undefined>(undefined);
  private readonly _baseUrl= environment.apiUrl + '/api/auth';
  public authState$ = this.token$.pipe(
    map(token => !!token)
  );
  public session$ = this._storage.get(sessionStorageKey) as unknown as Observable<ISession>;

  constructor(
    private readonly _http: HttpClient,
    private readonly _storage: StorageMap,
    private readonly _router: Router
  ) { }

  public async loginWithGoogle(googleIdToken: string): Promise<boolean> {
    const payload = JSON.parse(atob(googleIdToken.split('.')[1]));
    const url = `${this._baseUrl}/google-login`;
    const headers = new HttpHeaders().append('Authorization', `Bearer ${googleIdToken}`);
    return firstValueFrom(this._http.post<ISession>(url, {}, {headers})
      .pipe(tap(async authSession => {
        authSession.profilePicture = payload.picture;
        await this.saveSession(authSession);
      }))
      .pipe(map(() => {
        return true;
      })));
  }

  public async saveSession(authSession?: ISession): Promise<void> {
    if (authSession) {
      await firstValueFrom(this._storage.set(tokenStorageKey, authSession.token));
      await firstValueFrom(this._storage.set(sessionStorageKey, authSession));
    } else {
      await firstValueFrom(this._storage.delete(tokenStorageKey));
      await firstValueFrom(this._storage.delete(sessionStorageKey));
    }
    await this._loadSession();
  }

  private async _loadSession(): Promise<void> {
    const token = <string>await firstValueFrom(this._storage.get(tokenStorageKey));
    this.token$.next(token);
  }

  public async getOptions(needsAuth?: boolean): Promise<{ headers: HttpHeaders }> {
    return {headers: await this.getHeaders(needsAuth)};
  }

  public async hasRole(role: string): Promise<boolean> {
    const session = await this.getSession();
    if (!session || !session.role) {
      return false;
    }

    return session.role.indexOf(role) !== -1;
  }

  public async getHeaders(needsAuth?: boolean): Promise<HttpHeaders> {
    if (!needsAuth) {
      return new HttpHeaders();
    }
    const session = await this.getSession();

    if (!session) {
      return new HttpHeaders();
    }

    return new HttpHeaders().append('Authorization', `${session.tokenType} ${session.token}`);
  }

  public async getSession(): Promise<ISession> {
    return firstValueFrom(this.session$);
  }

  public get localAuthState(): boolean {
    return !!this.token$.value;
  }

  public get authStateAsync(): Promise<boolean> {
      if (this.localAuthState) {
        return Promise.resolve(true);
      } else {
        return this._loadSession().then(() => this.localAuthState);
      }
  }

  public async logout(): Promise<void> {
    await this.saveSession();
    await this._router.navigate(['/auth']);
  }
}

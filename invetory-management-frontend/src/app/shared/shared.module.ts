import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenuComponent} from './components/menu/menu.component';
import {ButtonComponent} from './components/button/button.component';
import {ButtonModule} from 'primeng/button';
import {WebcamModule} from 'ngx-webcam';
import {WebcamComponent} from './components/webcam/webcam.component';
import {SidebarModule} from 'primeng/sidebar';
import {PanelMenuModule} from 'primeng/panelmenu';
import {MenubarModule} from 'primeng/menubar';

@NgModule({
  declarations: [
    MenuComponent,
    ButtonComponent,
    WebcamComponent
  ],
  exports: [
    MenuComponent,
    ButtonComponent,
    WebcamComponent
  ],
    imports: [
        CommonModule,
        ButtonModule,
        WebcamModule,
        SidebarModule,
        PanelMenuModule,
        MenubarModule
    ]
})
export class SharedModule { }

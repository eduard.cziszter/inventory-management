import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})
export class ButtonComponent {
  @Input() label!: string;
  @Input() disabled!: boolean;
  @Input() type: string = 'grey-filled';
  @Input() loading!: boolean;
  @Input() icon!: string;
  @Input() size = 'big';
  @Output() readonly action = new EventEmitter<any>();
}

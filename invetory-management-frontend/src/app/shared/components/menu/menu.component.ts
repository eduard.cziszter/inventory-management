import {Component, HostListener, Inject, PLATFORM_ID} from '@angular/core';
import {MegaMenuItem, MenuItem} from 'primeng/api';
import {AuthService} from '../../../services/auth.service';
import {GoogleService} from '../../../services/google.service';
import {map, Observable, of, switchMap} from 'rxjs';
import {toSignal} from '@angular/core/rxjs-interop';
import {OrganizationsService} from '../../../services/organizations.service';
import {IOrganization} from '../../../models/organization.interface';
import {isPlatformBrowser} from '@angular/common';
import {constants} from '../../utils/constants';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss'
})
export class MenuComponent {
  readonly items$ = this._authService.authState$.pipe(
    switchMap(isLoggedIn => this._updateMenuItems(isLoggedIn))
  );
  readonly signalItems = toSignal(this.items$);
  screenWidth!: number;
  sideBarVisible = false;
  protected readonly constants = constants;

  constructor(
    private readonly _authService: AuthService,
    private readonly _googleService: GoogleService,
    private readonly _organizationsService: OrganizationsService,
    @Inject(PLATFORM_ID) private readonly _platformId: Object
  ) {
    if (isPlatformBrowser(_platformId)) {
      this.screenWidth = window.innerWidth;
    }
  }

  private _updateMenuItems(isLoggedIn: boolean): Observable<MenuItem[]> {
    if (!isLoggedIn) return of(this._getAllItems(isLoggedIn, []));
    return this._organizationsService.getOwn()
      .pipe(map((organizations) => this._getAllItems(isLoggedIn, organizations)));
  }

  private _getOrgItems(organizations: IOrganization[]): MenuItem[] {
    if (organizations && organizations.length > 0) {
      return organizations.map(org => (
        {label: org.shortName, routerLink: [`/organizations/${org.slug}`]}
      ));
    }
    return [];
  }

  private _getAllItems(isLoggedIn: boolean, organizations: IOrganization[]): MenuItem[] {
    return [
      {
        label: 'Autentificare cu Google',
        icon: 'pi pi-fw pi-google',
        visible: !isLoggedIn,
        command: () => this._googleService.googleLogin()
      },
      {
        label: 'Profil',
        icon: 'pi pi-fw pi-user',
        visible: isLoggedIn,
        routerLink: ['/profile'],
      },
      {
        label: 'Inventare',
        icon: 'pi pi-fw pi-sitemap',
        visible: isLoggedIn,
        items: this._getOrgItems(organizations),
      },
      {
        label: 'Cereri',
        icon: 'pi pi-fw pi-pen-to-square',
        visible: isLoggedIn,
        items: [
          {label: 'Cererile mele', routerLink: ['/requests']},
          {label: 'Aprobă cereri', routerLink: ['/requests/approve']},
        ]
      },
      {
        label: 'Logout',
        icon: 'pi pi-fw pi-sign-out',
        visible: isLoggedIn,
        styleClass: 'menu-item-end',
        command: () => this.logout()
      }
    ];
  }

  async logout(): Promise<void> {
    await this._authService.logout();
  }

  @HostListener('window:resize', ['$event'])
  getScreenWidth(): void {
    if (isPlatformBrowser(this._platformId)) {
      this.screenWidth = window.innerWidth;
    }
  }
}

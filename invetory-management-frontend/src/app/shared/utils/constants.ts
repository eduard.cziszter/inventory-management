export const constants = {
  firstBreakpoint: 1100,
  secondBreakpoint: 960,
  thirdBreakpoint: 600,
};

import {IItem} from './item.interface';

export interface IRequest {
  id: string;
  item: IItem;
  newItem?: IItem;
  requestType: string;
  description: string;
  approverEmail: string;
  needsApproval: boolean;
}

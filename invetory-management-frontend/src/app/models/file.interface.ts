export interface IFile {
  id: string;
  name: string;
  path: string;
  extension: string;
  originalName: string;
}

export interface IOrganization {
  id: string;
  slug: string;
  name: string;
  shortName: string;
  description: string;
  logoPath: string;
  departmentName: string;
}

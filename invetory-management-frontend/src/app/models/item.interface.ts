export interface IItem {
  id: string;
  name: string;
  description: string;
  quantity: number;
  imagePath: string;
  locationId: string;
  locationName: string;
  needsApproval: boolean;
  inventoryName: string;
}

export interface IItemAdd {
  name: string;
  description: string;
  imageId: string;
  locationId: string;
  inventoryId: string;
  quantity: number;
  needsApproval: boolean;
  requestDescription: string;
}

export interface IItemDelete {
  id: string;
  needsApproval: boolean;
  requestDescription: string;
}

export interface IItemTransfer {
  id: string;
  newQuantity: number;
  newInventoryId: string;
  newLocationId: string;
}

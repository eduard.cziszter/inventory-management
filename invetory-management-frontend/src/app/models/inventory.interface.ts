import {IItem} from './item.interface';

export interface IInventory {
  id: string;
  name: string;
  description: string;
  ownerId: string;
  ownerEmail: string;
  items: IItem[];
}

export interface IInventoryAdd {
  name: string;
  description: string;
  slug: string;
  organizationId: string;
}
